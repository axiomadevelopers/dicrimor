const pre_load ='<i class="fa fa-spinner fa-pulse fa-fw"></i>  Espere unos segundos mientras se carga la información';
//Cuerpo de funciones...
//
//funciones de validaciones
function valida(e,s,i,l)
{
  tecla = (document.all) ? e.keyCode : e.which;
  if (tecla==8 || tecla==0 || tecla==13) return true;
  //Exepcion barras y barras invertidas
  if(tecla == 47 || tecla == 92) return false;
  if (s.value.length>=l) return false;

  if (i==0) patron = /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]/;  // 0 Solo acepta letras
  if (i==1) patron = /[0123456789,.%]/;     // 1 Solo acepta n�meros
  if (i==2) patron = /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789]/;      // 2 Acepta n�meros y letras
  if (i==3) patron = /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789��������������\s]/;
  if (i==4) patron=  /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz��������������\s]/;
  if (i==5) patron=  /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789@._-]/; // Formato Correo Electronico
  if (i==6) patron=  /[ABCDEFabcdef0123456789]/;
  if (i==7) patron = /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789��������������()@:;_\-.,/\s]/;
  if (i==8) patron = /[01]/;
  if (i==9) patron = /[GJV0123456789]/; //Formato de RIF
  if (i==10)patron = /[0123456789]/;
  if (i==11)patron = /[abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_]/;
  if (i==12)patron = /[gjveGJVE0123456789]/;  //RIF
  if (i==13) patron = /[0123456789,]/;
  if (i==14) patron=  /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789._-]/; // Formato Nick Correo Electronico
  if (i==15) patron=  /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@.]/; // Formato direccion manual Correo Electronico
  if (i==16) patron = /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzáéíóúÁÉÍÓÚ\w]/;  // 0 Solo acepta letras y comas
  if (i==17) patron = /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789\s,.]/; // Acepta n�meros, letras, espacios ,.
  if (i==18) patron = /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\wáéíóúÁÉÍÓÚñÑ0123456789.,;()+-_=#*?¿{}$!\s]/; // Acepta n�meros, letras, espacios ,.
  if (i==19) patron=  /[A-Za-zñÑ'áéíóúÁÉÍÓÚàèìòùÀÈÌÒÙâêîôûÂÊÎÔÛÑñäëïöüÄËÏÖÜ\s\t]/;
  if (i==20) patron = /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789*.,;%()+-_=?¿{}$!]/; // Acepta clave para el ldap
  if (i==21) patron = /[+0123456789.()]/;
  if (i==22) patron=  /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789;_\-.,\s]/; // Formato de url de red social
  if (i==23) patron = /[+0123456789]/;
  if (i==24) patron = /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzáéíóúÁÉÍÓÚ\s,.]/; // Acepta letras, espacios ,.


  te = String.fromCharCode(tecla);
  return patron.test(te);
}
function valida2(s,i,l)
{
  if (i==0) patron = /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]/;  // 0 Solo acepta letras
  if (i==1) patron = /[0123456789,.%]/;     // 1 Solo acepta n�meros
  if (i==2) patron = /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789]/;      // 2 Acepta n�meros y letras
  if (i==3) patron = /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789��������������\s]/;
  if (i==4) patron=  /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz��������������\s]/;
  if (i==5) patron=  /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789@._-]/;
  if (i==6) patron=  /[ABCDEFabcdef0123456789]/;
  if (i==7) patron = /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789��������������()@:;_\-.,/\s]/;
  if (i==8) patron = /[01]/;
  if (i==9) patron = /[GJV0123456789]/;
  if (i==10)patron = /[0123456789]/;
  if (i==11)patron = /[abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_]/;
  if (i==12)patron = /[gjveGJVE0123456789]/;  //RIF
  if (i==13) patron = /[0123456789,]/;
  if (i==14) patron=  /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789._-]/; // Formato Nick Correo Electronico
  if (i==15) patron=  /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@.]/; // Formato direccion manual Correo Electronico
  if (i==16) patron = /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]/;  // 0 Solo acepta letras y comas
  if (i==17) patron = /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789\s,.]/; // 2 Acepta n�meros, letras, espacios ,.
  if (i==18) patron = /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\wáéíóúÁÉÍÓÚñÑ0123456789.,;()+-_=#*?¿{}$!\s]/; // Acepta n�meros, letras, espacios ,.
  if (i==19) patron=  /[A-Za-zñÑ'áéíóúÁÉÍÓÚàèìòùÀÈÌÒÙâêîôûÂÊÎÔÛÑñäëïöüÄËÏÖÜ\s]/;
  if (i==20) patron = /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789*.,;%()+-_=?¿{}$!]/; // Acepta clave para el ldap
  if (i==21) patron = /[+0123456789.()]/;
  if (i==22) patron=  /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789;_\-.,/\s]/; // Formato de url red social
  if (i==23) patron = /[+0123456789]/;
  if (i==24) patron = /[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzáéíóúÁÉÍÓÚ\s,.]/; // Acepta letras, espacios ,.

  r="";
  ll=0;
  for (i=0;i<s.value.length;i++)
  {
    if (patron.test(s.value.charAt(i)))
    {
      r=r+s.value.charAt(i);
      ll++;
      if (ll==l) break;
    }
  }

  return s.value=r;
}
//

//--
function correo(campo,campo_mensaje)
{
  var exr = /^\w+[a-z_0-9\-\.]+@\w+[0-9a-z\-\.]+\.[a-z]{2,4}$/i;
    if(!(exr.test(campo.value)))
    {
      campo.value = ""
      var idioma = $("#bandera").attr("data");
      switch(idioma){
        case "uk":
          id_idioma = 2
          break;
        case "es":
          id_idioma = 1
          break;  
      }
      if (id_idioma==true)
        mensaje_alerta("#"+campo_mensaje,"You must enter valid email direction", "alert-danger");
      else
        mensaje_alerta("#"+campo_mensaje,"Debe ingresar una dirección de correo v&aacute;lida", "alert-danger");

    }
}
/*
* No pegar
*/
function no_pegar(campo_mensaje){
  mensaje_alerta("#"+campo_mensaje,"No puede pegar en este campo", "alert-danger");
}
//--
function uploader_reg(campo,campos_bloquear)
{
    //alert(campo)
    $(campo).css({"display":"block"});
    $(campo).removeClass("alert alert-danger").removeClass("alert alert-success").addClass("alert alert-info alert-rounded mensaje_load");
    $(campo).html("");
    $(campo).html("<div class='alert-rounded'><i class='fa fa-spinner fa-pulse'></i> Por favor espere unos segundos mientras se ejecuta el proceso</div>");
    if(campos_bloquear!=""){
        bloquear_pantalla(campos_bloquear);
    }
}
//--Bloqueo campos de la pantalla
function bloquear_pantalla(campos){
    $(campos).attr("disabled","disabled");
}

//--Debloqueo de campos de la pantalla
function desbloquear_pantalla(campo_mensaje,campos){
    $(campos).prop("disabled",false);
    //$(campo_mensaje).html("");
    //$(campo_mensaje).removeClass("alert alert-info").removeClass("alert alert-danger").removeClass("alert alert-success mensaje_load");
}
//Mensaje de validacion
function mensaje_alerta(campo, mensaje, tipo_alerta)
{
  $(campo).removeClass("alert alert-danger").removeClass("alert alert-success").removeClass("alert alert-info").removeClass("alert alert-primary");
  $(campo).addClass("alert "+tipo_alerta);
  $(campo).html(mensaje);
  setTimeout(function()
  {
    $(campo).fadeOut(1400);
  },10000);
  $(campo).show();
}
function mensaje_alerta2(campo, mensaje, tipo_alerta)
{
  $(campo).removeClass("alert alert-danger").removeClass("alert alert-success").removeClass("alert alert-info").removeClass("alert alert-primary");
  $(campo).addClass("alert "+tipo_alerta);
  $(campo).html(mensaje);
  /*setTimeout(function()
  {
    $(campo).fadeOut(1400);
  },10000);
  $(campo).show();*/
}

//--
function ir_div(div){
    $('html,body').animate({
        scrollTop: $(div).offset().top
    }, 2000);
}
//--
function subir_top(){
  $('body, html').animate({
      scrollTop: '0px'
    }, 1000)
}
//-
function activar_parallax_inicio(){
  //subir_top();
  //setTimeout(function(){
  dzsprx_init('.dzsparallaxer', '{ direction: "normal", mode_scroll:"fromtop"}');
  //},40);
}
//
function cambiar_menu(){
   var scroll_start = 0;
   
   $(document).scroll(function() {
      scroll_start = $(this).scrollTop();

      if(screen.width>1024){
      //------------------------
        /*
        if(scroll_start > 20) {
           
            subtitulo_menu = "subtitulo-menu2"
            subtitulo_menu_ant = "subtitulo-menu"

        }else {
            subtitulo_menu = "subtitulo-menu"
            subtitulo_menu_ant = "subtitulo-menu2"
        }*/
        //alert("aqui>1024")
      //------------------------  
      }else{
        //alert("aqui<1024")
        /*
          if(scroll_start > 20) {
              $("#header_menu").addClass("fixed-top");
              subtitulo_menu = "subtitulo-menu2"
              subtitulo_menu_ant = "subtitulo-menu"
          }else{
              $("#header_menu").removeClass("fixed-top");
              subtitulo_menu = "subtitulo-menu"
              subtitulo_menu_ant = "subtitulo-menu2"
          }  
          */
      }
      //---
      /*
      *  Bloque para border bottom
      */
      url = window.location.href
      var base_url = $("#base_url").val();
      $(".span-menu").removeClass("subtitulo-menu").removeClass("subtitulo-menu2")

      switch(url){
        case base_url:
          $("#span-menu0").removeClass(subtitulo_menu_ant).addClass(subtitulo_menu)
          break;
        case base_url+'inicio':
          $("#span-menu0").removeClass(subtitulo_menu_ant).addClass(subtitulo_menu)
          break;
        case base_url+'home':
          $("#span-menu0").removeClass(subtitulo_menu_ant).addClass(subtitulo_menu)
          break;
        case base_url+'quienes_somos':
          $("#span-menu1").addClass("subtitulo-menu2")
          break;
        case base_url+'servicios':
          $("#span-menu3").addClass("subtitulo-menu2")
          break;
        case base_url+'productos':
          $("#span-menu2").addClass("subtitulo-menu2")
          break; 
        case base_url+'contactanos':
          $("#span-menu4").addClass("subtitulo-menu2")
          break; 
        default:
          url2 = url.replace(base_url,"");
          vec_ruta = url2.split("/")
          //--
          if(vec_ruta[0]=="productos"){
            $("#span-menu2").addClass("subtitulo-menu2")
          }else
          if(vec_ruta[0]=="servicios"){
            $("#span-menu3").addClass("subtitulo-menu2")
          }
          break;
          //--          
      }
       //---
   });
   
}
///
function activar_wow(){
  //--OBLIGATORIO PARA CORRER WOW-----------------------------------------------
    var wow = new WOW(
      {
        boxClass:     'wow',      // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset:       200,          // distance to the element when triggering the animation (default is 0)
        mobile:       false,       // trigger animations on mobile devices (default is true)
        live:         true,       // act on asynchronously loaded content (default is true)
        callback:     function(box) {
          // the callback is fired every time an animation is started
          // the argument that is passed in is the DOM node being animated
        }
      }
    );
    wow.init();
  //---
}

////////
var directionsDisplay,
directionsService,
map;
//--
function initialize_standar(lat,long,number){
  if((lat!=null)&&(long!=null)){
    //------------------------------------------------------------
    var latitud = parseFloat(lat)
    var longitud = parseFloat(long)
    var directionsService = new google.maps.DirectionsService();
    directionsDisplay = new google.maps.DirectionsRenderer();
    var myLatlng1 = new google.maps.LatLng(latitud,longitud);
    var mapOptions = {
      center: { lat: latitud, lng: longitud},
            zoom: 18,
            zoomControl: false,
            scaleControl: false,
            scrollwheel: false,
            disableDoubleClickZoom: true,
            draggable: false
    }
    var map = new google.maps.Map(document.getElementById("map_canvas"+number), mapOptions);


    directionsDisplay.setMap(map);
    var image = './assets/web/images/puntero uniseguros-01-01.png';
    var marker = new google.maps.Marker({
        position: myLatlng1,
        map: map,
        title: 'Uniseguros',
        icon: image
    });

    google.maps.event.addDomListener(window, 'resize', function() {
        map.panTo(myLatlng1);
    });
    //------------------------------------------------------------
  }
}
///////
//--------------------------
function init_slider(){
  //----------------------------------------
      var j = true;
      var i = "#owl-main";

      function a() {
          if (!j) {
              $(i + " .caption .fadeIn-1, " + i + " .caption .fadeIn-2, " + i + " .caption .fadeIn-3").stop().delay(800).animate({
                  opacity: 0
              }, {
                  duration: 400,
                  easing: "easeInCubic"
              })
          } else {
              $(i + " .caption .fadeIn-1, " + i + " .caption .fadeIn-2, " + i + " .caption .fadeIn-3").css({
                  opacity: 0
              })
          }
      }

      function c() {
          if (!j) {
              $(i + " .caption .fadeInDown-1, " + i + " .caption .fadeInDown-2, " + i + " .caption .fadeInDown-3").stop().delay(800).animate({
                  opacity: 0,
                  top: "-15px"
              }, {
                  duration: 400,
                  easing: "easeInCubic"
              })
          } else {
              $(i + " .caption .fadeInDown-1, " + i + " .caption .fadeInDown-2, " + i + " .caption .fadeInDown-3").css({
                  opacity: 0,
                  top: "-15px"
              })
          }
      }

      function l() {
          if (!j) {
              $(i + " .caption .fadeInUp-1, " + i + " .caption .fadeInUp-2, " + i + " .caption .fadeInUp-3").stop().delay(800).animate({
                  opacity: 0,
                  top: "15px"
              }, {
                  duration: 400,
                  easing: "easeInCubic"
              })
          } else {
              $(i + " .caption .fadeInUp-1, " + i + " .caption .fadeInUp-2, " + i + " .caption .fadeInUp-3").css({
                  opacity: 0,
                  top: "15px"
              })
          }
      }

      function b() {
          if (!j) {
              $(i + " .caption .fadeInLeft-1, " + i + " .caption .fadeInLeft-2, " + i + " .caption .fadeInLeft-3").stop().delay(800).animate({
                  opacity: 0,
                  left: "15px"
              }, {
                  duration: 400,
                  easing: "easeInCubic"
              })
          } else {
              $(i + " .caption .fadeInLeft-1, " + i + " .caption .fadeInLeft-2, " + i + " .caption .fadeInLeft-3").css({
                  opacity: 0,
                  left: "15px"
              })
          }
      }

      function e() {
          if (!j) {
              $(i + " .caption .fadeInRight-1, " + i + " .caption .fadeInRight-2, " + i + " .caption .fadeInRight-3").stop().delay(800).animate({
                  opacity: 0,
                  left: "-15px"
              }, {
                  duration: 400,
                  easing: "easeInCubic"
              })
          } else {
              $(i + " .caption .fadeInRight-1, " + i + " .caption .fadeInRight-2, " + i + " .caption .fadeInRight-3").css({
                  opacity: 0,
                  left: "-15px"
              })
          }
      }

      function f() {
          $(i + " .active .caption .fadeIn-1").stop().delay(500).animate({
              opacity: 1
          }, {
              duration: 800,
              easing: "easeOutCubic"
          });
          $(i + " .active .caption .fadeIn-2").stop().delay(700).animate({
              opacity: 1
          }, {
              duration: 800,
              easing: "easeOutCubic"
          });
          $(i + " .active .caption .fadeIn-3").stop().delay(1000).animate({
              opacity: 1
          }, {
              duration: 800,
              easing: "easeOutCubic"
          })
      }

      function g() {
          $(i + " .active .caption .fadeInDown-1").stop().delay(500).animate({
              opacity: 1,
              top: "0"
          }, {
              duration: 800,
              easing: "easeOutCubic"
          });
          $(i + " .active .caption .fadeInDown-2").stop().delay(700).animate({
              opacity: 1,
              top: "0"
          }, {
              duration: 800,
              easing: "easeOutCubic"
          });
          $(i + " .active .caption .fadeInDown-3").stop().delay(1000).animate({
              opacity: 1,
              top: "0"
          }, {
              duration: 800,
              easing: "easeOutCubic"
          })
      }

      function h() {
          $(i + " .active .caption .fadeInUp-1").stop().delay(500).animate({
              opacity: 1,
              top: "0"
          }, {
              duration: 800,
              easing: "easeOutCubic"
          });
          $(i + " .active .caption .fadeInUp-2").stop().delay(700).animate({
              opacity: 1,
              top: "0"
          }, {
              duration: 800,
              easing: "easeOutCubic"
          });
          $(i + " .active .caption .fadeInUp-3").stop().delay(1000).animate({
              opacity: 1,
              top: "0"
          }, {
              duration: 800,
              easing: "easeOutCubic"
          })
      }

      function k() {
          $(i + " .active .caption .fadeInLeft-1").stop().delay(500).animate({
              opacity: 1,
              left: "0"
          }, {
              duration: 800,
              easing: "easeOutCubic"
          });
          $(i + " .active .caption .fadeInLeft-2").stop().delay(700).animate({
              opacity: 1,
              left: "0"
          }, {
              duration: 800,
              easing: "easeOutCubic"
          });
          $(i + " .active .caption .fadeInLeft-3").stop().delay(1000).animate({
              opacity: 1,
              left: "0"
          }, {
              duration: 800,
              easing: "easeOutCubic"
          })
      }

      function d() {
          $(i + " .active .caption .fadeInRight-1").stop().delay(500).animate({
              opacity: 1,
              left: "0"
          }, {
              duration: 800,
              easing: "easeOutCubic"
          });
          $(i + " .active .caption .fadeInRight-2").stop().delay(700).animate({
              opacity: 1,
              left: "0"
          }, {
              duration: 800,
              easing: "easeOutCubic"
          });
          $(i + " .active .caption .fadeInRight-3").stop().delay(1000).animate({
              opacity: 1,
              left: "0"
          }, {
              duration: 800,
              easing: "easeOutCubic"
          })
      }
      $(i).owlCarousel({
          autoPlay: 5000,
          stopOnHover: true,
          navigation: true,
          pagination: true,
          singleItem: true,
          addClassActive: true,
          transitionStyle: "fade",
          navigationText: ["<i class='icon-left-open-mini'></i>", "<i class='icon-right-open-mini'></i>"],
          afterInit: function() {
              f();
              g();
              h();
              k();
              d()
          },
          afterMove: function() {
              f();
              g();
              h();
              k();
              d()
          },
          afterUpdate: function() {
              f();
              g();
              h();
              k();
              d()
          },
          startDragging: function() {
              j = true
          },
          afterAction: function() {
              a();
              c();
              l();
              b();
              e();
              j = false
          }
      });
      if ($(i).hasClass("owl-one-item")) {
          $(i + ".owl-one-item").data("owlCarousel").destroy()
      }
      $(i + ".owl-one-item").owlCarousel({
          singleItem: true,
          navigation: false,
          pagination: false
      });
      $("#transitionType li a").click(function() {
          $("#transitionType li a").removeClass("active");
          $(this).addClass("active");
          var m = $(this).attr("data-transition-type");
          $(i).data("owlCarousel").transitionTypes(m);
          $(i).trigger("owl.next");
          return false
      });
      $("#owl-testimonials").owlCarousel({
          autoPlay: 5000,
          stopOnHover: true,
          navigation: true,
          pagination: true,
          singleItem: true,
          addClassActive: true,
          autoHeight: true,
          transitionStyle: "fadeInAfterOut",
          navigationText: ["<i class='icon-left-open-mini'></i>", "<i class='icon-right-open-mini'></i>"]
      });
      $("#owl-projects").owlCarousel({
          navigation: false,
          autoHeight: true,
          slideSpeed: 300,
          paginationSpeed: 400,
          rewindNav: false,
          singleItem: true,
          navigationText: ["<i class='icon-left-open-mini'></i>", "<i class='icon-right-open-mini'></i>"]
      });
      $("#owl-latest-works").owlCarousel({
          autoPlay: 5000,
          stopOnHover: true,
          navigation: true,
          pagination: true,
          rewindNav: true,
          items: 4,
          navigationText: ["<i class='icon-left-open-mini'></i>", "<i class='icon-right-open-mini'></i>"]
      });
      $("#owl-videos").owlCarousel({
          autoPlay: 5000,
          stopOnHover: true,
          navigation: true,
          pagination: true,
          rewindNav: true,
          items: 5,
          navigationText: ["<i class='icon-left-open-mini'></i>", "<i class='icon-right-open-mini'></i>"]
      });
      $("#owl-audio").owlCarousel({
          autoPlay: 5000,
          stopOnHover: true,
          navigation: true,
          pagination: true,
          rewindNav: true,
          items: 5,
          navigationText: ["<i class='icon-left-open-mini'></i>", "<i class='icon-right-open-mini'></i>"]
      });
      $("#owl-popular-posts").owlCarousel({
          autoPlay: 5000,
          stopOnHover: true,
          navigation: true,
          pagination: true,
          rewindNav: true,
          items: 5,
          navigationText: ["<i class='icon-left-open-mini'></i>", "<i class='icon-right-open-mini'></i>"]
      });
      $("#owl-related-posts").owlCarousel({
          autoPlay: 5000,
          stopOnHover: true,
          navigation: true,
          pagination: true,
          rewindNav: true,
          items: 2,
          itemsDesktopSmall: [1199, 2],
          itemsTablet: [977, 2],
          navigationText: ["<i class='icon-left-open-mini'></i>", "<i class='icon-right-open-mini'></i>"]
      });
      $("#owl-featured-works").owlCarousel({
          autoPlay: 5000,
          stopOnHover: true,
          navigation: true,
          pagination: true,
          rewindNav: true,
          singleItem: true,
          transitionStyle: "goDown",
          navigationText: ["<i class='icon-left-open-mini'></i>", "<i class='icon-right-open-mini'></i>"]
      });
      $("#owl-work-samples").owlCarousel({
          autoPlay: 5000,
          stopOnHover: true,
          navigation: true,
          pagination: true,
          rewindNav: true,
          items: 3,
          itemsDesktopSmall: [1199, 3],
          itemsTablet: [977, 2],
          navigationText: ["<i class='icon-left-open-mini'></i>", "<i class='icon-right-open-mini'></i>"]
      });
      $("#owl-work-samples-big").owlCarousel({
          autoPlay: 5000,
          stopOnHover: true,
          navigation: true,
          pagination: true,
          rewindNav: true,
          singleItem: true,
          transitionStyle: "fadeUp",
          navigationText: ["<i class='icon-left-open-mini'></i>", "<i class='icon-right-open-mini'></i>"]
      });
      $("#owl-work, [id*='owl-work-modal']").owlCarousel({
          autoPlay: 5000,
          slideSpeed: 200,
          paginationSpeed: 600,
          rewindSpeed: 800,
          stopOnHover: true,
          navigation: true,
          pagination: true,
          rewindNav: true,
          singleItem: true,
          autoHeight: true,
          navigationText: ["<i class='icon-left-open-mini'></i>", "<i class='icon-right-open-mini'></i>"]
      });
      $("#owl-office").owlCarousel({
          autoPlay: 5000,
          slideSpeed: 200,
          paginationSpeed: 600,
          rewindSpeed: 800,
          stopOnHover: true,
          navigation: true,
          pagination: true,
          rewindNav: true,
          singleItem: true,
          autoHeight: true,
          transitionStyle: "fade",
          navigationText: ["<i class='icon-left-open-mini'></i>", "<i class='icon-right-open-mini'></i>"]
      });
      $("#owl-clients").owlCarousel({
          autoPlay: 5000,
          stopOnHover: true,
          rewindNav: true,
          items: 4,
          itemsDesktopSmall: [1199, 4],
          itemsTablet: [977, 3],
          navigation: true,
          pagination: true,
          navigationText: ["<i class='icon-left-open-mini'></i>", "<i class='icon-right-open-mini'></i>"]
      });
      $(".slider-next").click(function() {
          owl.trigger("owl.next")
      });
      $(".slider-prev").click(function() {
          owl.trigger("owl.prev")
      })
  //-----------------------------------------------------------------------------
}
//---------------------------
function init_portfolio(){
   var c = $(".items");
        c.imagesLoaded(function() {
            c.isotope({
                percentPosition: true,
                itemSelector: ".item",
                // masonry: {
                //     columnWidth: '50%'
                // }
            })
        });
        var a;

        function b() {
            c.isotope({
                itemSelector: ".item"
            });
            c.isotope('reloadItems');
        }
        b();
        $(window).resize(function() {
            clearTimeout(a);
            a = setTimeout(b, 100)
        });
        $("a.panel-toggle.collapsed").click(function() {
            clearTimeout(a);
            a = setTimeout(b, 100)
        });
        $(".portfolio .filter li a").click(function() {
            $(".portfolio .filter li a").removeClass("active");
            $(this).addClass("active");
            var d = $(this).attr("data-filter");
            c.isotope({
                filter: d
            });
            return false
        })
}
//---------------------------
function carga_inicio(){
   setTimeout(function(){
    activar_parallax_inicio();
    activar_wow();
    //init_portfolio();
    //$("#preloader").css({"display":"none"});
  },50);
}
//---------------------------
function validar_logos(){
  //---------------------------------
      if(screen.width<=360){
          $(".logo_inicial").attr({"src":"site_media/images/logo_mapa.png"});
      }else{
          $(".logo_inicial").attr({"src":"site_media/images/logo_ori.png"});
      }
  //---------------------------------
}
//---
function transformar_enlace(enlace){
  var material2=enlace.split("=");
  var material="https://www.youtube.com/v/"+material2[1];
  var enlace=ytVidId(material);
  enlace = "https://www.youtube.com/embed/"+enlace;
  return enlace;
}
//---
function transformar_enlace_miniatura(enlace){
  var material2=enlace.split("=");
  var material="https://www.youtube.com/v/"+material2[1];
  var enlace=ytVidId(material);
  enlace = "http://i1.ytimg.com/vi/"+enlace+"/hqdefault.jpg";
  return enlace;
}
//---
function ytVidId(url) {
  var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
  return (url.match(p)) ? RegExp.$1 : false;
}
//--
function cargar_enlace(){
  var enlace_url=$("#text_url_video").val();
  var material2=enlace_url.split("=");
  var material="https://www.youtube.com/v/"+material2[1];
  var enlace=ytVidId(material);
  if(enlace==false){
    mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Enlace de video no valido", "alert-danger");    
  }else{
    cargar_div_video(material);
  }
}
//--
function cargar_div_video(material_contenido){
  var previa='<object height="400px" width="80%" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">\
          <param name="movie" value="'+material_contenido+'">\
          <param name="wmode" value="transparent">\
          <embed src="'+material_contenido+'" type="application/x-shockwave-flash" wmode="transparent" height="400" width="80%">\
          </object>';
  $("#preview_material_multimedia").html(previa);
}

//---------------------------
///-Cuerpo llamados..
//cambiar_menu()
carga_inicio();

$('.productGallery').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
});

const base_url = $("#base_url").val();