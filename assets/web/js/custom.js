! function(e) {
    "use strict";
    e(window).on("load", function() {
        e(".layout").addClass("fade-in");
        $("#preloader_zougzoug").addClass("hide");
        var t = new WOW({
            mobile: !1
        });
        t.init()
    }), e(document).ready(function() {
        function t() {
            var t = e(".module-slides").superslides("current"),
                a = e(".slides-container li").eq(t);
            e(".slides-container li").removeClass("active-slide"), a.addClass("active-slide"), e(".slides-container li.bg-dark").hasClass("active-slide") ? (o.addClass("header-light"), e(".module-slides").removeClass("dark-nav")) : (o.removeClass("header-light"), e(".module-slides").addClass("dark-nav"))
        }

        function a() {
            e("li.bg-dark").hasClass("flex-active-slide") ? (o.addClass("header-light"), e(".flexslider").removeClass("dark-nav")) : (o.removeClass("header-light"), e(".flexslider").addClass("dark-nav"))
        }

        function i() {
            var e = document.createElement("p");
            e.style.width = "100%", e.style.height = "200px";
            var t = document.createElement("div");
            t.style.position = "absolute", t.style.top = "0px", t.style.left = "0px", t.style.visibility = "hidden", t.style.width = "200px", t.style.height = "150px", t.style.overflow = "hidden", t.appendChild(e), document.body.appendChild(t);
            var a = e.offsetWidth;
            t.style.overflow = "scroll";
            var i = e.offsetWidth;
            return a == i && (i = t.clientWidth), document.body.removeChild(t), a - i
        }
        e(".sticky-sidebar").imagesLoaded(function() {
            e(".sticky-sidebar").stick_in_parent({
                offset_top: 80,
                recalc_every: 1
            }).on("sticky_kit:bottom", function(t) {
                e(this).parent().css("position", "relative")
            }).on("sticky_kit:unbottom", function(t) {
                e(this).parent().css("position", "relative")
            }).scroll()
        });
        var s;
        s = !!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
        var o = e(".header");
        e(window).scroll(function() {
            var t = e(window).scrollTop();
            t >= 20 ? (o.addClass("header-small"), o.addClass("header-shadow")) : (o.removeClass("header-small"), o.removeClass("header-shadow")), e(".module-header").length <= 0 && e(".module-slides").length <= 0 && e(".flexslider").length <= 0 && o.addClass("header-small")
        }).scroll();
        var n = e(".module-header");
        n.length >= 0 && (n.hasClass("bg-dark") ? o.addClass("header-light") : o.removeClass("header-light")), e(".onepage-nav").singlePageNav({
            currentClass: "active",
            filter: ":not(.external)"
        }), e(document).on("click", ".inner-navigation.show", function(t) {
            e(t.target).is("a") && !e(t.target).parent().hasClass("menu-item-has-children") && e(this).collapse("hide")
        }), 1 != s && e(".ripples").length > 0 && e(".ripples").each(function() {
            e(this).ripples(e.extend({
                resolution: 500,
                dropRadius: 30,
                perturbance: .04
            }, e(this).data("ripples-options")))
        }), e(".slides-container li").length > 1 ? (e(".module-slides").superslides({
            play: 1e4,
            animation: "fade",
            animation_speed: 800,
            pagination: !0
        }), e(document).on("animated.slides", function() {
            t()
        })) : 1 == e(".slides-container li").length && (e(".module-slides").superslides(), t()), e(".flexslider").length > 0 && e(".flexslider").flexslider({
            animation: "fade",
            animationSpeed: 1e3,
            slideshowSpeed: 9e3,
            animationLoop: !0,
            prevText: " ",
            nextText: " ",
            start: function(e) {
                a()
            },
            before: function(t) {
                1 != s && (e(".flexslider .container").fadeOut(500).animate({
                    opacity: "0"
                }, 500), t.slides.eq(t.currentSlide).delay(500), t.slides.eq(t.animatingTo).delay(500))
            },
            after: function(t) {
                a(), 1 != s && e(".flexslider .container").fadeIn(500).animate({
                    opacity: "1"
                }, 500)
            },
            useCSS: !0
        }), e(".rotate").textrotator({
            animation: "dissolve",
            separator: "|",
            speed: 3e3
        }), e("[data-background]").each(function() {
            e(this).css("background-image", "url(" + e(this).attr("data-background") + ")")
        }), e("[data-background-color]").each(function() {
            e(this).css("background-color", e(this).attr("data-background-color"))
        }), e("[data-color]").each(function() {
            e(this).css("color", e(this).attr("data-color"))
        }), e("[data-mY]").each(function() {
            e(this).css("margin-top", e(this).attr("data-mY"))
        });
        var r = e("#filters"),
            l = e(".row-portfolio");
        e("a", r).on("click", function() {
            var t = e(this).attr("data-filter");
            return e(".current", r).removeClass("current"), e(this).addClass("current"), l.isotope({
                filter: t
            }), !1
        }), e(window).on("resize", function() {
            l.imagesLoaded(function() {
                l.isotope({
                    layoutMode: "masonry",
                    itemSelector: ".portfolio-item, .portfolio-item-classic ",
                    transitionDuration: "0.4s",
                    masonry: {
                        columnWidth: ".grid-sizer"
                    }
                })
            })
        }).resize(), e(window).on("resize", function() {
            setTimeout(function() {
                e(".blog-masonry").isotope({
                    layoutMode: "masonry",
                    transitionDuration: "0.8s"
                })
            }, 1e3)
        }), e(window).on("resize", function() {
            e(".row-shop-masonry").isotope({
                layoutMode: "masonry",
                transitionDuration: "0.4s"
            })
        }), e(".open-offcanvas, .close-offcanvas").on("click", function() {
            return e("body").toggleClass("off-canvas-sidebar-open"), !1
        }), e(document).on("click", function(t) {
            var a = e(".off-canvas-sidebar");
            a.is(t.target) || 0 !== a.has(t.target).length || e("body").removeClass("off-canvas-sidebar-open")
        }), e(".off-canvas-sidebar-wrapper").css("margin-right", "-" + i() + "px"), e(window).on("resize", function() {
            var t = Math.max(e(window).width(), window.innerWidth);
            t <= 991 && e("body").removeClass("off-canvas-sidebar-open")
        }), e(".clients-carousel").each(function() {
            e(this).owlCarousel(e.extend({
                navigation: !1,
                pagination: !1,
                autoPlay: 3e3,
                items: 4,
                navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
            }, e(this).data("carousel-options")))
        }), e(".tms-carousel").each(function() {
            e(this).owlCarousel(e.extend({
                navigation: !1,
                pagination: !0,
                autoPlay: 3e3,
                items: 3,
                navigationText: ['<span class="arrows arrows-arrows-slim-left"></span>', '<span class="arrows arrows-arrows-slim-right"></span>']
            }, e(this).data("carousel-options")))
        }), e(".tms-slides").each(function() {
            e(this).owlCarousel(e.extend({
                autoPlay: 5e3,
                navigation: !1,
                singleItem: !0,
                pagination: !0,
                paginationSpeed: 1e3,
                navigationText: ['<span class="arrows arrows-arrows-slim-left"></span>', '<span class="arrows arrows-arrows-slim-right"></span>']
            }, e(this).data("carousel-options")))
        }), e(".image-slider").each(function() {
            e(this).owlCarousel(e.extend({
                stopOnHover: !0,
                navigation: !0,
                pagination: !0,
                autoPlay: 3e3,
                singleItem: !0,
                items: 1,
                navigationText: ['<span class="arrows arrows-arrows-slim-left"></span>', '<span class="arrows arrows-arrows-slim-right"></span>']
            }, e(this).data("carousel-options")))
        }), /*e(".progress-bar").each(function() {
            e(this).appear(function() {
                var t = e(this).attr("aria-valuenow");
                e(this).animate({
                    width: t + "%"
                }), e(this).find(".pb-number-box").animate({
                    opacity: 1
                }, 1e3), e(this).find(".pb-number").countTo({
                    from: 0,
                    to: t,
                    speed: 900,
                    refreshInterval: 30
                })
            })
        }), e(".counter-timer").each(function() {
            e(this).appear(function() {
                var t = e(this).attr("data-to");
                e(this).countTo({
                    from: 0,
                    to: t,
                    speed: 1500,
                    refreshInterval: 10,
                    formatter: function(e, t) {
                        return e = e.toFixed(t.decimals), e = e.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                })
            })
        }), e(".chart").each(function() {
            e(this).appear(function() {
                e(this).easyPieChart(e.extend({
                    barColor: "#111111",
                    trackColor: "#eee",
                    scaleColor: !1,
                    lineCap: "round",
                    lineWidth: 5,
                    size: 180
                }, e(this).data("chart-options")));
                var t = e(this).attr("data-percent");
                e(this).find(".chart-text span").countTo({
                    from: 0,
                    to: t,
                    speed: 1e3,
                    refreshInterval: 10,
                    formatter: function(e, t) {
                        return e = e.toFixed(t.decimals), e = e.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                })
            })
        }),*/ e(".twitter-feed").each(function(t) {
            e(this).attr("id", "twitter-" + t);
            var a = e(this).data("twitter"),
                i = e(this).data("number"),
                s = {
                    id: a,
                    domId: "twitter-" + t,
                    maxTweets: i,
                    enableLinks: !0,
                    showPermalinks: !1
                };
            twitterFetcher.fetch(s)
        }), e(".lightbox").magnificPopup({
            type: "image"
        }), e("[rel=single-photo]").magnificPopup({
            type: "image",
            gallery: {
                enabled: !0,
                navigateByImgClick: !0,
                preload: [0, 1]
            }
        }), e(".gallery [rel=gallery]").magnificPopup({
            type: "image",
            gallery: {
                enabled: !0,
                navigateByImgClick: !0,
                preload: [0, 1]
            },
            image: {
                titleSrc: "title",
                tError: "The image could not be loaded."
            }
        }), e(".lightbox-video").magnificPopup({
            type: "iframe"
        }), e("a.product-gallery").magnificPopup({
            type: "image",
            gallery: {
                enabled: !0,
                navigateByImgClick: !0,
                preload: [0, 1]
            },
            image: {
                titleSrc: "title",
                tError: "The image could not be loaded."
            }
        }), e(function() {
            e('[data-toggle="tooltip"]').tooltip()
        }), /*e("body").fitVids(), e(".map").each(function() {
            var t = /\[[^(\]\[)]*\]/g,
                a = e(this),
                i = Math.max(e(window).width(), window.innerWidth) > 736;
            if (a.length > 0) {
                var s, o = a[0].getAttribute("data-addresses").match(t),
                    n = a[0].getAttribute("data-info").match(t),
                    r = a.data("icon"),
                    l = a.data("zoom"),
                    d = [];
                o.forEach(function(e, t) {
                    var a = "{";
                    if (a += '"latLng":' + e, 0 == t && (s = JSON.parse(e)), n[t]) {
                        var i = n[t].replace(/\[|\]/g, "");
                        a += ', "data":"' + i + '"'
                    }
                    a += "}", d.push(JSON.parse(a))
                });
                var c = {
                    scrollwheel: !1,
                    styles: [{
                        featureType: "water",
                        elementType: "geometry",
                        stylers: [{
                            color: "#e9e9e9"
                        }, {
                            lightness: 17
                        }]
                    }, {
                        featureType: "landscape",
                        elementType: "geometry",
                        stylers: [{
                            color: "#f5f5f5"
                        }, {
                            lightness: 20
                        }]
                    }, {
                        featureType: "road.highway",
                        elementType: "geometry.fill",
                        stylers: [{
                            color: "#ffffff"
                        }, {
                            lightness: 17
                        }]
                    }, {
                        featureType: "road.highway",
                        elementType: "geometry.stroke",
                        stylers: [{
                            color: "#ffffff"
                        }, {
                            lightness: 29
                        }, {
                            weight: .2
                        }]
                    }, {
                        featureType: "road.arterial",
                        elementType: "geometry",
                        stylers: [{
                            color: "#ffffff"
                        }, {
                            lightness: 18
                        }]
                    }, {
                        featureType: "road.local",
                        elementType: "geometry",
                        stylers: [{
                            color: "#ffffff"
                        }, {
                            lightness: 16
                        }]
                    }, {
                        featureType: "poi",
                        elementType: "geometry",
                        stylers: [{
                            color: "#f5f5f5"
                        }, {
                            lightness: 21
                        }]
                    }, {
                        featureType: "poi.park",
                        elementType: "geometry",
                        stylers: [{
                            color: "#dedede"
                        }, {
                            lightness: 21
                        }]
                    }, {
                        elementType: "labels.text.stroke",
                        stylers: [{
                            visibility: "on"
                        }, {
                            color: "#ffffff"
                        }, {
                            lightness: 16
                        }]
                    }, {
                        elementType: "labels.text.fill",
                        stylers: [{
                            saturation: 36
                        }, {
                            color: "#333333"
                        }, {
                            lightness: 40
                        }]
                    }, {
                        elementType: "labels.icon",
                        stylers: [{
                            visibility: "off"
                        }]
                    }, {
                        featureType: "transit",
                        elementType: "geometry",
                        stylers: [{
                            color: "#f2f2f2"
                        }, {
                            lightness: 19
                        }]
                    }, {
                        featureType: "administrative",
                        elementType: "geometry.fill",
                        stylers: [{
                            color: "#fefefe"
                        }, {
                            lightness: 20
                        }]
                    }, {
                        featureType: "administrative",
                        elementType: "geometry.stroke",
                        stylers: [{
                            color: "#fefefe"
                        }, {
                            lightness: 17
                        }, {
                            weight: 1.2
                        }]
                    }]
                };
                c.center = s, c.zoom = l, c.draggable = i;
                var p = {};
                p.icon = r, a.gmap3({
                    map: {
                        options: c
                    },
                    marker: {
                        values: d,
                        options: p,
                        events: {
                            click: function(t, a, i) {
                                if (i.data) {
                                    var s = e(this).gmap3("get"),
                                        o = e(this).gmap3({
                                            get: {
                                                name: "infowindow"
                                            }
                                        });
                                    o ? (o.open(s, t), o.setContent(i.data)) : e(this).gmap3({
                                        infowindow: {
                                            anchor: t,
                                            options: {
                                                content: i.data
                                            }
                                        }
                                    })
                                }
                            }
                        }
                    }
                })
            }
        }),*/ e("#particles-js").length > 0 && particlesJS("particles-js", {
            particles: {
                number: {
                    value: 60,
                    density: {
                        enable: !0,
                        value_area: 900
                    }
                },
                color: {
                    value: "#ffffff"
                },
                shape: {
                    type: "circle",
                    stroke: {
                        width: 0,
                        color: "#ffffff"
                    },
                    polygon: {
                        nb_sides: 5
                    },
                    image: {
                        src: "img/github.svg",
                        width: 100,
                        height: 100
                    }
                },
                opacity: {
                    value: .3,
                    random: !0,
                    anim: {
                        enable: !0,
                        speed: 1,
                        opacity_min: .1,
                        sync: !1
                    }
                },
                size: {
                    value: 5,
                    random: !0,
                    anim: {
                        enable: !1,
                        speed: 40,
                        size_min: .1,
                        sync: !1
                    }
                },
                line_linked: {
                    enable: !0,
                    distance: 150,
                    color: "#ffffff",
                    opacity: .4,
                    width: 1
                },
                move: {
                    enable: !0,
                    speed: 1,
                    direction: "top",
                    random: !0,
                    straight: !1,
                    out_mode: "out",
                    bounce: !1,
                    attract: {
                        enable: !0,
                        rotateX: 600,
                        rotateY: 1200
                    }
                }
            },
            interactivity: {
                detect_on: "canvas",
                events: {
                    onhover: {
                        enable: !0,
                        mode: "grab"
                    },
                    onclick: {
                        enable: !0,
                        mode: "repulse"
                    },
                    resize: !0
                },
                modes: {
                    grab: {
                        distance: 260,
                        line_linked: {
                            opacity: .6
                        }
                    },
                    bubble: {
                        distance: 400,
                        size: 40,
                        duration: 2,
                        opacity: 8,
                        speed: 3
                    },
                    repulse: {
                        distance: 200,
                        duration: .4
                    },
                    push: {
                        particles_nb: 4
                    },
                    remove: {
                        particles_nb: 2
                    }
                }
            },
            retina_detect: !0
        }), e("#particles-snow-js").length > 0 && particlesJS("particles-snow-js", {
            particles: {
                number: {
                    value: 400,
                    density: {
                        enable: !0,
                        value_area: 800
                    }
                },
                color: {
                    value: "#fff"
                },
                shape: {
                    type: "circle",
                    stroke: {
                        width: 0,
                        color: "#000000"
                    },
                    polygon: {
                        nb_sides: 5
                    },
                    image: {
                        src: "img/github.svg",
                        width: 100,
                        height: 100
                    }
                },
                opacity: {
                    value: .5,
                    random: !0,
                    anim: {
                        enable: !1,
                        speed: 1,
                        opacity_min: .1,
                        sync: !1
                    }
                },
                size: {
                    value: 7,
                    random: !0,
                    anim: {
                        enable: !1,
                        speed: 40,
                        size_min: .1,
                        sync: !1
                    }
                },
                line_linked: {
                    enable: !1,
                    distance: 500,
                    color: "#ffffff",
                    opacity: .4,
                    width: 2
                },
                move: {
                    enable: !0,
                    speed: 6,
                    direction: "bottom",
                    random: !1,
                    straight: !1,
                    out_mode: "out",
                    bounce: !1,
                    attract: {
                        enable: !1,
                        rotateX: 600,
                        rotateY: 1200
                    }
                }
            },
            interactivity: {
                detect_on: "canvas",
                events: {
                    onhover: {
                        enable: !0,
                        mode: "bubble"
                    },
                    onclick: {
                        enable: !0,
                        mode: "repulse"
                    },
                    resize: !0
                },
                modes: {
                    grab: {
                        distance: 400,
                        line_linked: {
                            opacity: .5
                        }
                    },
                    bubble: {
                        distance: 400,
                        size: 4,
                        duration: .3,
                        opacity: 1,
                        speed: 3
                    },
                    repulse: {
                        distance: 200,
                        duration: .4
                    },
                    push: {
                        particles_nb: 4
                    },
                    remove: {
                        particles_nb: 2
                    }
                }
            },
            retina_detect: !0
        }), e(".smoothscroll").on("click", function(t) {
            var a = this.hash,
                i = e(a);
            e("html, body").stop().animate({
                scrollTop: i.offset().top - o.height()
            }, 600, "swing"), t.preventDefault()
        }), e(window).scroll(function() {
            e(this).scrollTop() > 100 ? e(".scroll-top").addClass("scroll-top-visible") : e(".scroll-top").removeClass("scroll-top-visible")
        }), e('a[href="#top"]').on("click", function() {
            return e("html, body").animate({
                scrollTop: 0
            }, "slow"), !1
        });
        var d, c = document.body;
        window.addEventListener("scroll", function() {
            clearTimeout(d), c.classList.contains("disable-hover") || c.classList.add("disable-hover"), d = setTimeout(function() {
                c.classList.remove("disable-hover")
            }, 100)
        }, !1), e(".parallax").jarallax({
            speed: .4
        })
    })
}(jQuery);