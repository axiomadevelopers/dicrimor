angular.module("menuApp")
	.controller("mainController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,footerFactory){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();
		
		$scope.productos = {
							'0':{
									id:"1",
									titulo:"Ice Cream Roll",
									imagen:$scope.base_url+"assets/web/images/productos/icream.png",
									slug:"productos/ice_cream_roll",
								},

							'1':{
								id:"2",
								titulo:"Brownie",
								imagen:$scope.base_url+"assets/web/images/productos/brownie.png",
								slug:"productos/brownie_helado",
							},

							'2':{
								id:"3",
								titulo:"Choco",
								imagen:$scope.base_url+"assets/web/images/productos/choco.png",
								slug:"productos/choco_chips",
							},

							'3':{
								id:"4",
								titulo:"Barquirolls",
								imagen:$scope.base_url+"assets/web/images/productos/barqui.png",
								slug:"productos/barquirolls",
							},
		}
		console.log($scope.productos)
	});