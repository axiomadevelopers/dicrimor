angular.module("menuApp")
	.controller("productosController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,footerFactory){
		//--Cuerpo de declaraciones
		$scope.menu = []
        $scope.url = []
        $scope.producto_seleccionado = []
		$scope.base_url = $("#base_url").val();
        $scope.slug = document.getElementById('slug_producto').getAttribute('value');
		$scope.productos = {
                    '0':{
                            id:"1",
                            titulo:"Ice Cream Roll",
                            imagen:$scope.base_url+"assets/web/images/productos/icream.png",
                            slug:"productos/ice_cream_roll",
                            ref:'Ref.dc-ic1',
                            precio:"5$",
                            descripcion:["6 Rolls del sabor de tu elección","1 Syrup o 1 Topping Crocante"],
                        },

                    '1':{
                        id:"2",
                        titulo:"Barquirolls",
                        imagen:$scope.base_url+"assets/web/images/productos/barqui.png",
                        slug:"productos/barquirolls",
                        ref:'Ref.dc-br1',
                        precio:"4$",
                        descripcion:["3 Rolls grandes","1 Cono de galleta","1 Syrup de tu elección"],
                        descripcion2:["Borde: Lluvia de chocolate colores, maní o coco","Punta rellena: Chocolate o dulce de leche"],
                    },

                    '2':{
                        id:"3",
                        titulo:"Brownie Helado",
                        imagen:$scope.base_url+"assets/web/images/productos/brownie.png",
                        slug:"productos/brownie_helado",
                        ref:'Ref.dc-bh1',
                        precio:"5$",
                        descripcion:["3 Rolls grandes","1 Brownie","1 Syrup de tu elección","Crema chantilly"],
                    },
                    /*
                    '3':{
                        id:"4",
                        titulo:"Waffle Helado",
                        imagen:$scope.base_url+"assets/web/images/productos/barqui.png",
                        slug:"productos/waffle_helado",
                        ref:'Ref.dc-bh1',
                        precio:"5$",
                        descripcion:["1 Waffle grande","4 Rolls grandes","1 Syrup de tu elección"],
                    },*/
                    '4':{
                        id:"5",
                        titulo:"Choco Chips",
                        imagen:$scope.base_url+"assets/web/images/productos/choco.png",
                        slug:"productos/choco_chips",
                        ref:'Ref.dc-bh1',
                        precio:"5$",
                        descripcion:["1 Waffle grande","4 Rolls grandes","1 Syrup de tu elección"],
                    },
        }
        //Seleccion d eproducto segun slug
        switch($scope.slug){
            
            case 'ice_cream_roll':
                $scope.producto_seleccionado = $scope.productos[0];
                break;

            case 'barquirolls':
                $scope.producto_seleccionado = $scope.productos[1];
                break;
            
            case 'brownie_helado':
                $scope.producto_seleccionado = $scope.productos[2];
                break;    
            
            case 'choco_chips':
                $scope.producto_seleccionado = $scope.productos[4];
                break;
            default:
                break;    
        }
        
		console.log($scope.producto_seleccionado)
	});