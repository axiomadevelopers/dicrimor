angular.module("menuApp")
	.controller("contactanosController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,nosotrosFactory,directivaFactory,reaseguradorasFactory,cuentaBancariaFactory,cargaPdfFactory){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();
		$("#span-menu4").addClass("subtitulo-menu2")
		$scope.idioma = 1
		$scope.contactos = {
                                'id':'',
                                'nombres':'',
                                'email':'',
                                'telefono':'',
                                'mensaje':''
        }
		//---------------------------------------------------------------------
		//--
		$scope.validar_guardar = function(){
			if($scope.contactos.nombres==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar sus nombres", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter their names", "alert-danger");

				return false;
			}else
			if($("#form-email").val()==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar una dirección de correo v&aacute;lida", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter valid email direction", "alert-danger");
				return false;
			}
			else
			if($("#form-tlf").val()==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar un número de teléfono válido", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter valid telephone number", "alert-danger");
				return false;
			}
			else if($scope.contactos.mensaje==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar un mensaje", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter a message", "alert-danger");
				return false;
			}else
				return true;
		}
		//--
		//--Registrar contactos
		$scope.registrar_contactos = function(){
			if($scope.validar_guardar()==true){
				uploader_reg("#campo_mensaje_clientes",".campos-form");
				$http.post($scope.base_url+"/WebContactos/registrarContactos",{
					'nombres': $scope.contactos.nombres,
					'email': $scope.contactos.email,
					'telefono': $scope.contactos.telefono,
					'mensaje': $scope.contactos.mensaje
				}).success(function(data, estatus, headers, config){
					$scope.mensajes  = data;
					if($scope.mensajes.mensaje == "registro_procesado"){
						if($scope.idioma == "1")
							mensaje_alerta("#campo_mensaje_clientes","Gracias por tu tiempo, tu mensaje ha sido enviado!","alert-success");
						else
							mensaje_alerta("#campo_mensaje_clientes","Thank you for your time, your message has been sent!","alert-success");
						$scope.limpiar_cajas();
					}else if($scope.mensajes.mensaje =="existe"){
						if($scope.idioma == "1")
							mensaje_alerta("#campo_mensaje_clientes","Ya fue registrado un contacto con esa dirección de email","alert-success");
						else
							mensaje_alerta("#campo_mensaje_clientes","A contact has already been registered with that email address","alert-success");
					}
					else{
						mensaje_alerta("#campo_mensaje_clientes","Ocurrió un error inesperado","alert-danger");

					}
					desbloquear_pantalla("#campo_mensaje_clientes",".campos-form")
				}).error(function(data,estatus){
					showErrorMessage("Ocurrió un error inesperado:"+data);
				})
			}
		}

		//--Limpiar cajas
		$scope.limpiar_cajas = function(){
			$scope.contactos = {
									"id":"",
									"nombres":"",
									"email":"",
									"telefono":"",
									"mensaje":""
			}
		}
		//-- consultar_direccion
		$scope.consultar_direccion = function(){
			direccionFactory.asignar_valores("","",$scope.base_url)
			direccionFactory.cargar_direccion(function(data){
				$scope.direccion=data;
				//console.log($scope.direccion);
				$scope.direccion_mapas = $scope.direccion[0]
			  	$scope.direccion_mapas_telefono = $scope.direccion_mapas.telefono
			  	//---------------------------------------------------------------
			  	var latitud= $scope.direccion_mapas.latitud;  
				var longitud= $scope.direccion_mapas.longitud;
				var numero = 0;
				google.maps.event.addDomListener(window, 'load', initialize_standar(latitud,longitud,numero));
				setTimeout(function(){
					$("#listaLocalizacion0").addClass("alert-success")
				},5000)
			});
		}
		/*
		*	preSeleccionarRenglon
		*/
		$scope.preSeleccionarRenglon = function(indice){
			//---
			$( ".lista-localizacion" ).each(function( index ) {
			  	if(!$(this).hasClass("alert-success")){
			  		$(this).removeClass("alert-info")
				}
			});
			//---
			if(!$(this).hasClass("alert-success")){
		  		$("#listaLocalizacion"+indice).addClass("alert-info")
		  	}
		  	//---	
		}
		/*
		*	seleccionaRenglon
		*/
		$scope.seleccionaRenglon = function(indice){
			//---
			$( ".lista-localizacion" ).each(function( index ) {
				$(this).removeClass("alert-info").removeClass("alert-success")
			});
			//---
		  	$("#listaLocalizacion"+indice).addClass("alert-success")
		  	$scope.direccion_mapas = $scope.direccion[indice]
		  	$scope.direccion_mapas_telefono = $scope.direccion_mapas.telefono
		  	//---------------------------------------------------------------
		  	var latitud= $scope.direccion_mapas.latitud;  
			var longitud= $scope.direccion_mapas.longitud;
			var numero = 0;
			google.maps.event.addDomListener(window, 'load', initialize_standar(latitud,longitud,numero));
		  	//---------------------------------------------------------------
		  	//console.log($scope.direccion_mapas)
		}
		/*
		*	Carga de cuentas bancarias
		*/
		$scope.consultar_banco = function(){
			cuentaBancariaFactory.asignar_valores("","",$scope.base_url)
			cuentaBancariaFactory.cargar_banco(function(data){
				$scope.banco=data;
				//console.log($scope.banco);				
			});
		}
		/*
		*	consultar_pdf
		*/
		$scope.consultar_pdf = function(){
			cargaPdfFactory.asignar_valores("","30",$scope.base_url)
			cargaPdfFactory.cargar_banco(function(data){
				$scope.pdf=data[0];
				console.log($scope.pdf);				
			});
		}

		/*
		*	desSeleccionarRenglon
		*/
		$scope.desSeleccionarRenglon = function(indice){
			$("#listaLocalizacion"+indice).removeClass("alert-info")
		}
		//--
		$scope.consultar_direccion()
		$scope.consultar_banco()
		$scope.consultar_pdf()
		//--
		//---------------------------------------------------------------------
	});