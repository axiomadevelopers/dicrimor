angular.module("menuApp")
	.controller("detalleController", function($scope,$sce,$http,$location,direccionFactory,redessocialesFactory,metaFactory,noticiasFactory,noticiasLimiteFactory,categoriasWebFactory,publicidadFactory,bucadorTitulo){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();
		$scope.idioma = 1
		$scope.slug  = $("#slug").val();
		$scope.publicidades = []
		
		//--Cuerpo de metodos
		$scope.consultar_publicidad = function(){
			publicidadFactory.asignar_valores("","",$scope.base_url)
			publicidadFactory.cargar_publicidad(function(data){
				$scope.publicidades=data;
				console.log($scope.publicidades);
			});
		}
		//--
		$scope.consultar_categorias = function(){
			categoriasWebFactory.asignar_valores("","",$scope.base_url)
			categoriasWebFactory.cargar_categorias(function(data){
				$scope.categorias_blog=data;
			});
		}
		//--
		$scope.consultar_post = function(){
			$scope.editorial = $("#editorial").val()
			noticiasFactory.asignar_valores("1","",$scope.editorial,$scope.slug,$scope.base_url)
			noticiasFactory.cargar_noticias(function(data){
				$scope.detalle_posts=data[0];
				
				console.log($scope.editorial);
				
				if($scope.detalle_posts.link_youtube!=""){
					$scope.cargar_video()
				}

				url = $scope.trustSrc()
				url = 'https://w.soundcloud.com/player/?url='+url;
				$("#iframe").prop("src",url)
				$scope.consultar_otros_post()
			})
		}
		//
		$scope.consultar_otros_post = function(){
			noticiasLimiteFactory.asignar_valores("1","3","0",$scope.detalle_posts.id,$scope.base_url)
			noticiasLimiteFactory.cargar_noticias_limite(function(data){
				$scope.post_limites=data;
				console.log($scope.post_limites);
			})
		}
		//--
		$scope.cargar_video = function(){
			video_link = transformar_enlace($scope.detalle_posts.link_youtube)
			$("#reproductor").attr({"src":video_link});
			//console.log($scope.sub_videos);
		}
		//--
		$scope.trustSrc = function() {
			return $sce.trustAsResourceUrl($scope.detalle_posts.link_soundcloud);
		}
		//--
		$scope.buscarTitulos = function(){
			alert($scope.base_url);
        	bucadorTitulo.buscarT($scope.base_url);
        }
        //--
        $scope.buscarTitulosEnter = function(event){
        	 if (event.keyCode === 13) {
		       bucadorTitulo.buscarT($scope.base_url);
		    }
        }
        //--
		$scope.contactos = {
                                'id':'',
                                'nombres':'',
                                'email':'',
                                'telefono':'',
                                'mensaje':''
        }
        
		$scope.direccion_mapas = []
		$scope.direccion_mapas_telefono = []
		//---------------------------
		$scope.consultar_publicidad()
		$scope.consultar_categorias()
		$scope.consultar_post()
	});