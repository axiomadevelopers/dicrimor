angular.module("menuApp")
	.controller("buscadorController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,noticiasFactoryBuscador,categoriasWebFactory,publicidadFactory,bucadorTitulo){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();
		$scope.id = $("#id_filtro").val();
		$scope.tipo = $("#tipo_filtro").val();
		$scope.idioma = 1
		$scope.limit = "2"
		$scope.start = "0"
		$scope.mensaje = ""
		$scope.posts_buscador_mas = []
		$scope.posts_buscador =	{
								'titulo':'',
								'descripcion':'',
								'img':'',
								'ruta':'',
								'autor':'',
								'fecha':'',
								'dias':'',
								'mes':'',
								'usuario':'',
						}
		//--Cuerpo de metodos
		$scope.consultar_publicidad = function(){
			publicidadFactory.asignar_valores("","",$scope.base_url)
			publicidadFactory.cargar_publicidad(function(data){
				$scope.publicidades=data;
			});
		}
		//--
		$scope.consultar_categorias = function(){
			categoriasWebFactory.asignar_valores("","",$scope.base_url)
			categoriasWebFactory.cargar_categorias(function(data){
				$scope.categorias_blog=data;
			});
		}
		//--
		$scope.consultar_post_buscador = function(){
			noticiasFactoryBuscador.asignar_valores("1",$scope.start,$scope.limit,$scope.tipo,$scope.id,$scope.base_url)
			noticiasFactoryBuscador.cargar_noticias_buscador(function(data){
				//console.log(data[0])
				if(data[0]==undefined){
					$scope.mensaje = "No existen registros asociados!"
				}else{
					$scope.posts_buscador=data;
					//console.log($scope.posts_buscador);
					$scope.start += 1;
				}
			})
		}
		//--
		$scope.carga_noticias_leer_mas = function(){
			noticiasFactoryBuscador.asignar_valores("1",$scope.start,$scope.limit,$scope.tipo,$scope.id,$scope.base_url)
			noticiasFactoryBuscador.cargar_noticias_buscador(function(data){
			//----------------------------------------
				//console.log(data[0])
				if (data === null || data == "null" || data[0]==undefined) {
						mensaje_alerta("#campo_mensaje_noticias","Todos los post fueron cargadas!","alert-warning");
				} else {
					for (i in data) {
						$scope.posts_buscador_mas.push(data[i]);
					}
					//console.log($scope.posts_buscador_mas)
					$scope.start += 1;
				}
			//----------------------------------------
			})
		}
		//--
		$scope.cargar_mas_subnoticias = function(){
			$.when(
				mensaje_alerta("#campo_mensaje_noticias",pre_load,"alert-warning"),
				$scope.carga_noticias_leer_mas()
			).then(function(){
				 $("#campo_mensaje_noticias").fadeOut(1400)
				 $("#campo_mensaje_noticias").show()
			});
		}
		//--
		$scope.buscarTitulos = function(){
        	bucadorTitulo.buscarT($scope.base_url);
        }
		//--
		$scope.contactos = {
                                'id':'',
                                'nombres':'',
                                'email':'',
                                'telefono':'',
                                'mensaje':''
        }
        $scope.consultar_publicidad()
        $scope.consultar_categorias()
		$scope.consultar_post_buscador() 
		$scope.direccion_mapas = []
		$scope.direccion_mapas_telefono = []
		//---------------------------
	})
	.controller("contactanosController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,nosotrosFactory,directivaFactory,reaseguradorasFactory,cuentaBancariaFactory,cargaPdfFactory){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();
		$("#span-menu4").addClass("subtitulo-menu2")
		$scope.idioma = 1
		$scope.contactos = {
                                'id':'',
                                'nombres':'',
                                'email':'',
                                'telefono':'',
                                'mensaje':''
        }
		//---------------------------------------------------------------------
		//--
		$scope.validar_guardar = function(){
			if($scope.contactos.nombres==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar sus nombres", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter their names", "alert-danger");

				return false;
			}else
			if($("#form-email").val()==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar una dirección de correo v&aacute;lida", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter valid email direction", "alert-danger");
				return false;
			}
			else
			if($("#form-tlf").val()==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar un número de teléfono válido", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter valid telephone number", "alert-danger");
				return false;
			}
			else if($scope.contactos.mensaje==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar un mensaje", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter a message", "alert-danger");
				return false;
			}else
				return true;
		}
		//--
		//--Registrar contactos
		$scope.registrar_contactos = function(){
			if($scope.validar_guardar()==true){
				uploader_reg("#campo_mensaje_clientes",".campos-form");
				$http.post($scope.base_url+"/WebContactos/registrarContactos",{
					'nombres': $scope.contactos.nombres,
					'email': $scope.contactos.email,
					'telefono': $scope.contactos.telefono,
					'mensaje': $scope.contactos.mensaje
				}).success(function(data, estatus, headers, config){
					$scope.mensajes  = data;
					if($scope.mensajes.mensaje == "registro_procesado"){
						if($scope.idioma == "1")
							mensaje_alerta("#campo_mensaje_clientes","Gracias por tu tiempo, tu mensaje ha sido enviado!","alert-success");
						else
							mensaje_alerta("#campo_mensaje_clientes","Thank you for your time, your message has been sent!","alert-success");
						$scope.limpiar_cajas();
					}else if($scope.mensajes.mensaje =="existe"){
						if($scope.idioma == "1")
							mensaje_alerta("#campo_mensaje_clientes","Ya fue registrado un contacto con esa dirección de email","alert-success");
						else
							mensaje_alerta("#campo_mensaje_clientes","A contact has already been registered with that email address","alert-success");
					}
					else{
						mensaje_alerta("#campo_mensaje_clientes","Ocurrió un error inesperado","alert-danger");

					}
					desbloquear_pantalla("#campo_mensaje_clientes",".campos-form")
				}).error(function(data,estatus){
					showErrorMessage("Ocurrió un error inesperado:"+data);
				})
			}
		}

		//--Limpiar cajas
		$scope.limpiar_cajas = function(){
			$scope.contactos = {
									"id":"",
									"nombres":"",
									"email":"",
									"telefono":"",
									"mensaje":""
			}
		}
		//-- consultar_direccion
		$scope.consultar_direccion = function(){
			direccionFactory.asignar_valores("","",$scope.base_url)
			direccionFactory.cargar_direccion(function(data){
				$scope.direccion=data;
				//console.log($scope.direccion);
				$scope.direccion_mapas = $scope.direccion[0]
			  	$scope.direccion_mapas_telefono = $scope.direccion_mapas.telefono
			  	//---------------------------------------------------------------
			  	var latitud= $scope.direccion_mapas.latitud;  
				var longitud= $scope.direccion_mapas.longitud;
				var numero = 0;
				google.maps.event.addDomListener(window, 'load', initialize_standar(latitud,longitud,numero));
				setTimeout(function(){
					$("#listaLocalizacion0").addClass("alert-success")
				},5000)
			});
		}
		/*
		*	preSeleccionarRenglon
		*/
		$scope.preSeleccionarRenglon = function(indice){
			//---
			$( ".lista-localizacion" ).each(function( index ) {
			  	if(!$(this).hasClass("alert-success")){
			  		$(this).removeClass("alert-info")
				}
			});
			//---
			if(!$(this).hasClass("alert-success")){
		  		$("#listaLocalizacion"+indice).addClass("alert-info")
		  	}
		  	//---	
		}
		/*
		*	seleccionaRenglon
		*/
		$scope.seleccionaRenglon = function(indice){
			//---
			$( ".lista-localizacion" ).each(function( index ) {
				$(this).removeClass("alert-info").removeClass("alert-success")
			});
			//---
		  	$("#listaLocalizacion"+indice).addClass("alert-success")
		  	$scope.direccion_mapas = $scope.direccion[indice]
		  	$scope.direccion_mapas_telefono = $scope.direccion_mapas.telefono
		  	//---------------------------------------------------------------
		  	var latitud= $scope.direccion_mapas.latitud;  
			var longitud= $scope.direccion_mapas.longitud;
			var numero = 0;
			google.maps.event.addDomListener(window, 'load', initialize_standar(latitud,longitud,numero));
		  	//---------------------------------------------------------------
		  	//console.log($scope.direccion_mapas)
		}
		/*
		*	Carga de cuentas bancarias
		*/
		$scope.consultar_banco = function(){
			cuentaBancariaFactory.asignar_valores("","",$scope.base_url)
			cuentaBancariaFactory.cargar_banco(function(data){
				$scope.banco=data;
				//console.log($scope.banco);				
			});
		}
		/*
		*	consultar_pdf
		*/
		$scope.consultar_pdf = function(){
			cargaPdfFactory.asignar_valores("","30",$scope.base_url)
			cargaPdfFactory.cargar_banco(function(data){
				$scope.pdf=data[0];
				//console.log($scope.pdf);				
			});
		}

		/*
		*	desSeleccionarRenglon
		*/
		$scope.desSeleccionarRenglon = function(indice){
			$("#listaLocalizacion"+indice).removeClass("alert-info")
		}
		//--
		$scope.consultar_direccion()
		$scope.consultar_banco()
		$scope.consultar_pdf()
		//--
		//---------------------------------------------------------------------
	})
	.controller("detalleController", function($scope,$sce,$http,$location,direccionFactory,redessocialesFactory,metaFactory,noticiasFactory,noticiasLimiteFactory,categoriasWebFactory,publicidadFactory,bucadorTitulo){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();
		$scope.idioma = 1
		$scope.slug  = $("#slug").val();
		
		//--Cuerpo de metodos
		$scope.consultar_publicidad = function(){
			publicidadFactory.asignar_valores("","",$scope.base_url)
			publicidadFactory.cargar_publicidad(function(data){
				$scope.publicidades=data;
			});
		}
		//--
		$scope.consultar_categorias = function(){
			categoriasWebFactory.asignar_valores("","",$scope.base_url)
			categoriasWebFactory.cargar_categorias(function(data){
				$scope.categorias_blog=data;
			});
		}
		//--
		$scope.consultar_post = function(){
			$scope.editorial = $("#editorial").val()
			noticiasFactory.asignar_valores("","",$scope.editorial,$scope.slug,$scope.base_url)
			noticiasFactory.cargar_noticias(function(data){
				$scope.detalle_posts=data[0];
				
				//console.log($scope.detalle_posts);
				
				if($scope.detalle_posts.link_youtube!="")
					$scope.cargar_video()
				url = $scope.trustSrc()
				url = 'https://w.soundcloud.com/player/?url='+url;
				$("#iframe").prop("src",url)
				$scope.consultar_otros_post()
			})
		}
		//
		$scope.consultar_otros_post = function(){
			noticiasLimiteFactory.asignar_valores("1","3","0",$scope.detalle_posts.id,$scope.base_url)
			noticiasLimiteFactory.cargar_noticias_limite(function(data){
				$scope.post_limites=data;
				//console.log($scope.post_limites);
			})
		}
		//--
		$scope.cargar_video = function(){
			video_link = transformar_enlace($scope.detalle_posts.link_youtube)
			$("#reproductor").attr({"src":video_link});
			//console.log($scope.sub_videos);
		}
		//--
		$scope.trustSrc = function() {
			return $sce.trustAsResourceUrl($scope.detalle_posts.link_soundcloud);
		}
		//--
		$scope.buscarTitulos = function(){
        	bucadorTitulo.buscarT($scope.base_url);
        }
        //--
		$scope.contactos = {
                                'id':'',
                                'nombres':'',
                                'email':'',
                                'telefono':'',
                                'mensaje':''
        }
        
		$scope.direccion_mapas = []
		$scope.direccion_mapas_telefono = []
		//---------------------------
		$scope.consultar_publicidad()
		$scope.consultar_categorias()
		$scope.consultar_post()
	})
	.controller("footerController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,footerFactory){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();

		$scope.consultar_redes = function () {
			footerFactory.asignar_valores("","",$scope.base_url);
			footerFactory.cargar_redes(function (data) {
				$scope.redes = data;
				$scope.instagram =$scope.redes[0].url_red;
				$scope.facebook =$scope.redes[1].url_red;
				$scope.twitter =$scope.redes[2].url_red;
				$scope.linkedin = "";
				//$scope.redes[3].url_red;
			});
		}

		$scope.consultarFooter = function(){
			footerFactory.asignar_valores("","",$scope.base_url)
			footerFactory.cargar_footer(function(data){
				$scope.footer=data[0];
			});
		}
		//---
		$scope.consultar_meta = function () {
			footerFactory.cargar_meta_tag(function (data) {
				$scope.meta_descripcion = data.descripcion;
				$scope.meta_palabras_claves = data.palabras_claves;

				$("meta[name='keywords']").attr('content', $scope.meta_palabras_claves);
				$("meta[name='description']").attr('content', $scope.meta_descripcion);
				//console.log(data);
			});
		}
		//---
		$scope.consultar_direccion = function(){
			direccionFactory.asignar_valores("","",$scope.base_url)
			direccionFactory.cargar_direccion(function(data){
				$scope.direccion=data[0];
				//console.log($scope.direccion)
			});
		}
		//--
		$scope.consultar_redes()
		$scope.consultar_direccion()
		$scope.consultarFooter()
		//--
	})
	.controller("inicioController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,portadaFactory,empresaFactory,categoriasWebFactory,publicidadFactory,noticiasFactory,bucadorTitulo){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();
		$scope.idioma = 1
		let img_editorial = $scope.base_url+'assets/web/images/home/slider1.jpeg'
		let img_noticias = $scope.base_url+'assets/web/images/home/slider3.jpeg'
		
		let publicidad = $scope.base_url+'assets/web/images/publicidad/publicidad1.jpg'
		let publicidad2 = $scope.base_url+'assets/web/images/publicidad/publicidad2.png'
		//--Cuerpo de metodos
		$scope.consultar_publicidad = function(){
			publicidadFactory.asignar_valores("","",$scope.base_url)
			publicidadFactory.cargar_publicidad(function(data){
				$scope.publicidades=data;
				console.log($scope.publicidades);
			});
		}
		//--
		$scope.consultar_categorias = function(){
			categoriasWebFactory.asignar_valores("","",$scope.base_url)
			categoriasWebFactory.cargar_categorias(function(data){
				$scope.categorias_blog=data;
			});
		}
		//--
		$scope.consultar_empresa = function(){
			empresaFactory.asignar_valores("","",$scope.base_url)
			empresaFactory.cargar_categorias(function(data){
				$scope.empresa=data[0];
			});
		}
		//--
		$scope.consultar_portada = function(){
			portadaFactory.asignar_valores("",$scope.base_url)
			portadaFactory.cargar_categorias(function(data){
				$scope.portada=data[0];
			});
		}
		//--
		$scope.consultar_editorial = function(){
			noticiasFactory.asignar_valores("","","1","",$scope.base_url)
			noticiasFactory.cargar_noticias(function(data){
				$scope.editorial=data[0];
				//console.log($scope.editorial)
			})
		}
		//--
		$scope.consultar_post = function(){
			noticiasFactory.asignar_valores("","","","",$scope.base_url)
			noticiasFactory.cargar_noticias(function(data){
				$scope.posts=data;
			})
		}
		//--
        $scope.buscarTitulos = function(){
        	bucadorTitulo.buscarT($scope.base_url);
        }
       
		$scope.direccion_mapas = []
		$scope.direccion_mapas_telefono = []
		//---------------------------
		$scope.consultar_publicidad()
		$scope.consultar_categorias()
		$scope.consultar_empresa()
		$scope.consultar_portada()
		$scope.consultar_editorial()
		$scope.consultar_post()
	})
	.controller("mainController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,footerFactory){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();

		
	})