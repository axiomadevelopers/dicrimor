angular.module("menuApp")
	.controller("footerController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,footerFactory,categoriasWebFactory,empresaFactory){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();
		let direccion_html=" <p>"+
								"Avenida Principal de Lechería,"+
							"</p>"+
							"<p>"+
								"C.C. Anna, Piso 1, Local 24, Lecherí­a."+
							"</p>"+
							"<p>"+
								"Anzoategui - Venezuela"+
							"</p>";	
							
			$scope.productos = {
								'0':{
										id:"1",
										titulo:"Ice Cream Roll",
										imagen:$scope.base_url+"assets/web/images/productos/ice_cream_roll.jpg",
										slug:"productos/ice_cream_roll",
									},
	
								'1':{
									id:"2",
									titulo:"Espirales",
									imagen:$scope.base_url+"assets/web/images/productos/espirales.jpg",
									slug:"productos/espirales",
								},
	
								'2':{
									id:"3",
									titulo:"Barqui Roll",
									imagen:$scope.base_url+"assets/web/images/productos/barqui_roll.jpg",
									slug:"productos/barqui_roll",
								},
	
								'3':{
									id:"4",
									titulo:"Barqui Pirullin",
									imagen:$scope.base_url+"assets/web/images/productos/barquipirulin.jpg",
									slug:"productos/barquipirullin",
								},
		}	

		$scope.footer = {
			titulo:'Dicrimor',
			direccion : direccion_html,
			telefono:"(+58) 4248018246",
			email:"dicrimor@gmail.com",
		}
		//--
	});