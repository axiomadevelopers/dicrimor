//----------
//Cuerpo de funciones


function cargar_en_ingles(){
	var base_url = $("#base_url").val()
	//--Cambiar de nombre a menu
	$(".menu0").text("Inicio");
	$(".menu1").text("Quienes Somos");
	$(".menu2").text("Productos");
	$(".menu3").text("Contáctanos");
	$(".menu4").text("Servicios");

	//--Cambiar rutas
	$(".a-menu0").attr("href",base_url+"inicio");
	$(".a-menu1").attr("href",base_url+"quienes_somos");
	$(".a-menu2").attr("href",base_url+"servicios");
	$(".a-menu3").attr("href",base_url+"contactanos");
	$(".a-menu4").attr("href",base_url+"noticias");
	//--CTA
	$(".cta-contactanos").text("CONTACTANOS");
	$(".cta-servicios").text("SERVICIOS");

	
	//$("#btn_slide3").text("Ir a servicios");

	//--Cambiar de nombre a titulos parallax
	//$("#parallax1").text("NUESTROS CLIENTES");
	$("#parallax2").text("SERVICIOS");
	$("#parallax3").text("BANCOS ALIADOS");
	$("#parallax4").text("CONTÁCTANOS");
	$("#parallax5").text("CLIENTES");
	$("#parallax6").text("NOTICIAS");
	$("#parallax8").text("REDES SOCIALES");
	$("#parallax9").text("QUIENES SOMOS");
	//--Para el home:
	//--Seccion 1


	$("#form-nombre").attr("placeholder","NOMBRE");
	$("#form-tlf").attr("placeholder","TELÉFONO");
	$("#form-email").attr("placeholder","EMAIL");
	$("#form-mensaje").attr("placeholder","MENSAJE");
	$(".btn-enviar").text("ENVIAR");
	//--Detalle servicios
	$(".titulo_como_funciona").text("CÓMO FUNCIONA");
	$("#titulo-comof1-serv").text("Gestión Cuentas Por Pagar Proveedores ");
	$("#parrafo-comof1-serv").text("Si eres cliente de nuestros Bancos Aliados, a través del sistema de Tufactoring podrás canalizar el pago de tus obligaciones con tus proveedores por medio de la aceptación de un servicio de Confirming con o sin financiamiento. ");
	  	  
	$("#titulo-comof2-serv").text("Venta de Facturas (Factoring)");
	$("#parrafo-comof2-serv").text("Tus facturas  por cobrar a los clientes que actualmente se encuentren en el sistema de Tufactoring, podrás autorizar  que sean publicadas en subasta online y recibir de potenciales inversionistas ofertas de compras con diferentes porcentaje de descuentos. ");
	
	$("#titulo-comof3-serv").text("Compra de Facturas ");
	$("#parrafo-comof3-serv").text("El proceso de subasta te permitirá realizar compras de facturas sin límite de inversión, con la confianza de pago por la existencia de un banco aliado que te confirmará que pagará al vencimiento de la factura. Al cierre de la jornada de la subasta, el inversionista que comprará la factura será el que mejor oferta de descuenta presente al vendedor.");
	//--Parrafos como funciona
	$("#parrafo-comof1-serv-gen").text("Debes registrar toda la Información que solicita el sistema de Tufactoring para que tengas acceso a los  términos y condiciones del contrato que debes otorgar al banco. Una vez autorizado por el banco se realizará  de forma automática  la carga  diaria de tus cuentas por pagar al sistema de Tufactoring y así  el banco podrá gestionar  los procesos de notificación y cancelación al vencimiento de las obligaciones pendientes");
	$(".parrafo-comof2-serv-gen").text("Puedes ser invitado por un cliente a través del sistema o ingresar directamente para que registres toda la Información que solicita el sistema de Tufactoring. La aprobación por parte del banco aliado  es indispensable para que puedas acceder posteriormente a los contratos electrónicos de factoring sin recurso que otorgarás a los inversionistas que compren tus facturas en los procesos de subasta. En caso de que no desees participar en la subasta, el pago de tu factura lo recibirás del banco aliado al vencimiento de la factura");
	$("#parrafo-comof3-serv-gen").text("Debes registrar toda la Información que solicita el sistema de Tufactoring para que tengas acceso a los  términos y condiciones para operar. Debes poseer una cuenta en el sistema bancario y una vez hayas sido autorizado por nuestros bancos aliados, tendrás acceso diario a las subastas de facturas para que durante las jornadas puedas ofertar  sobre todas la facturas publicadas. En caso de ser  adjudicada la compra, recibirás contrato de factoring sin recurso otorgado por el vendedor  y la confirmación de pago al vencimiento de la factura (que hará el banco). El pago de la compra lo podrás realizar por medio de  botón de pago si tu cuenta esta con el banco aliado o con transferencia si tu cuenta pertenece a otra institución.");
	//---Seccion de noticias
	$("#otras-noticias").html("Otras noticias");
	if($("#tipo_noticia").text()=="detalle"){
		window.location = base_url+"noticias"
	}
	//contenido noticia 1
	$("#titulo_not_1").text("Titulo 1");
	$("#img_not_1").attr("src",base_url+"assets/web/images/news/news1.jpg");
	$("#fecha_not_1").text("Diciembre 6, 2019");
	$("#usuario_not_1").text("Ususario 1");
	$("#des_not_1").text("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet leo in enim tempor suscipit eu eu mi. 					Praesent id porttitor quam. Vivamus vitae varius ex, in molestie metus");
	$("#des_cor_not_1").text("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet leo in enim tempor");
	$(".slug_not_1").attr("href",base_url+"noticias/titulo-noticias-1");

	//contenido noticia 2
	$("#titulo_not_2").text("Titulo 2");
	$("#img_not_2").attr("src",base_url+"assets/web/images/news/news2.jpg");
	$("#fecha_not_2").text("Diciembre 7, 2019");
	$("#usuario_not_2").text("Ususario 2");
	$("#des_not_2").text("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet leo in enim tempor suscipit eu eu mi. 					Praesent id porttitor quam. Vivamus vitae varius ex, in molestie metus");
	$("#des_cor_not_2").text("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet leo in enim tempor");
	$(".slug_not_2").attr("href",base_url+"noticias/titulo-noticias-2");
	
	//contenido noticia 3
	$("#titulo_not_3").text("Titulo 3");
	$("#img_not_3").attr("src",base_url+"assets/web/images/news/news3.jpg");
	$("#fecha_not_3").text("Diciembre 8, 2019");
	$("#usuario_not_3").text("Ususario 3");
	$("#des_not_3").text("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet leo in enim tempor suscipit eu eu mi. 					Praesent id porttitor quam. Vivamus vitae varius ex, in molestie metus");
	$("#des_cor_not_3").text("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet leo in enim tempor");
	$(".slug_not_3").attr("href",base_url+"noticias/titulo-noticias-3");

	//contenido noticia 4
	$("#titulo_not_4").text("Titulo 4");
	$("#img_not_4").attr("src",base_url+"assets/web/images/news/news4.jpg");
	$("#fecha_not_4").text("Diciembre 9, 2019");
	$("#usuario_not_4").text("Ususario 4");
	$("#des_not_4").text("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet leo in enim tempor suscipit eu eu mi. 					Praesent id porttitor quam. Vivamus vitae varius ex, in molestie metus");
	$("#des_cor_not_4").text("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet leo in enim tempor");
	$(".slug_not_4").attr("href",base_url+"noticias/titulo-noticias-4");

	//Para Noticias detalles
	$idioma_noticia_detalle = $("#idioma_noticia_detalle").val();
	$slug_noticia = $(".slug_noticia").val();
	titulo_not = "";
	//--Switch
	switch($slug_noticia){
		case 'titulo-noticias-1':
			titulo_not = "Titulo 1";
			imagen_not = base_url+"assets/web/images/news/news1.jpg";
			fecha_not = "Diciembre 6, 2019";
			usuario_not = "Ususario 1";
			des_not = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet leo in enim tempor suscipit eu eu mi. 					Praesent id porttitor quam. Vivamus vitae varius ex, in molestie metus";
			des_cor_not = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet leo in enim tempor";
		break;
		case 'titulo-noticias-2':
			titulo_not = "Titulo 2";
			imagen_not = base_url+"assets/web/images/news/news2.jpg";
			fecha_not = "Diciembre 7, 2019";
			usuario_not = "Ususario 2";
			des_not = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet leo in enim tempor suscipit eu eu mi. 					Praesent id porttitor quam. Vivamus vitae varius ex, in molestie metus";
			des_cor_not = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet leo in enim tempor";
		break;
		case 'titulo-noticias-3':
			titulo_not = "Titulo 3";
			imagen_not = base_url+"assets/web/images/news/news3.jpg";
			fecha_not = "Diciembre 8, 2019";
			usuario_not = "Ususario 3";
			des_not = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet leo in enim tempor suscipit eu eu mi. 					Praesent id porttitor quam. Vivamus vitae varius ex, in molestie metus";
			des_cor_not = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet leo in enim tempor";
		break;
		case 'titulo-noticias-4':
			titulo_not = "Titulo 4";
			imagen_not = base_url+"assets/web/images/news/news4.jpg";
			fecha_not = "Diciembre 9, 2019";
			usuario_not = "Ususario 4";
			des_not = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet leo in enim tempor suscipit eu eu mi. 					Praesent id porttitor quam. Vivamus vitae varius ex, in molestie metus";
			des_cor_not = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet leo in enim tempor";
		break;	
	}
	//--
	//Asigno los valores
	if(titulo_not!=""){
		$("#titulo_not_desc").text(titulo_not);
		$("#img_not_desc").attr("src",imagen_not);
		$("#fecha_not_desc").text(fecha_not);
		$("#usuario_not_desc").text(usuario_not);
		$("#des_not_desc").text(des_not);
		$("#tipo_noticia").text("detalle")
		$(".cta-otras-noticias").text("Otras noticias")
	}
	//--Footer
	//--
}

function cambio_idioma_case(){
	//---------------------------------
	var base_url = $("#base_url").val();
	var tipo_idioma = $("#id_idioma").text()
	if(tipo_idioma==1)
		idioma=2
	else
		idioma=1 
	//---------------------------------
	url = window.location.href
	switch(url){
		case base_url:
			if(idioma==1)
				window.location.href = base_url+"inicio"
			else
				window.location.href = base_url+"home"
			break;
		case base_url+'inicio':
			if(idioma==1)
				window.location.href = base_url+"inicio"
			else
				window.location.href = base_url+"home"
			break;
		case base_url+'home':
			if(idioma==1)
				window.location.href = base_url+"inicio"
			else
				window.location.href = base_url+"home"
			break;
		case base_url+'quienes_somos':
			if(idioma==1)
				window.location.href = base_url+"quienes_somos"
			else
				window.location.href = base_url+"about"
			break;
		case base_url+'about':
			if(idioma==1)
				window.location.href = base_url+"quienes_somos"
			else
				window.location.href = base_url+"about"
			break;
		case base_url+'servicios':
			if(idioma==1)
				window.location.href = base_url+"servicios"
			else
				window.location.href = base_url+"services"
			break;
		case base_url+'services':
			if(idioma==1)
				window.location.href = base_url+"servicios"
			else
				window.location.href = base_url+"services"
			break;
		case base_url+'portafolio':
			if(idioma==1)
				window.location.href = base_url+"portafolio"
			else
				window.location.href = base_url+"portfolio"
			break;
		case base_url+'portfolio':
			if(idioma==1)
				window.location.href = base_url+"portafolio"
			else
				window.location.href = base_url+"portfolio"
			break;
		case base_url+'noticias':
			if(idioma==1)
				window.location.href = base_url+"noticias"
			else
				window.location.href = base_url+"news"
			break;
		case base_url+'news':
			if(idioma==1)
				window.location.href = base_url+"noticias"
			else
				window.location.href = base_url+"news"
			break;		
		case base_url+'contactanos':
			if(idioma==1)
				window.location.href = base_url+"contactos"
			else
				window.location.href = base_url+"contact"
			break;
		case base_url+'contact':
			if(idioma==1)
				window.location.href = base_url+"contactos"
			else
				window.location.href = base_url+"contact"
			break;		
		default:
			url2 = url.replace(base_url,"");
			vec_ruta = url2.split("/")
			//--
			if(vec_ruta[0]=="news"){
				window.location.href = base_url+"noticias"
			}else
			if(vec_ruta[0]=="noticias"){
				window.location.href = base_url+"news"
			}/*else
			if((vec_ruta[1]=="blog")&&(vec_ruta[2]=="es")){
				$location.path("/blog/en/")
			}else if((vec_ruta[1]=="blog")&&(vec_ruta[2]=="en")){
				$location.path("/blog/es/")
			}*/
			break;
			//--					
	}
}

function cambio_idioma (){
	cambio_idioma_case()
	/*var idioma = $("#bandera").attr("data");
	switch(idioma){
		case "uk":
			cargar_en_espaniol();
			break;
		case "es":
			cargar_en_ingles();
			break;	
	}*/
}

//--Limpiar cajas del form
function limpiar_cajas(){
	$(".campos-form").val("");
}
//---Validar formulario de enviar
function validar_guardar(){
	//--Defino el idioma
	var idioma = $("#bandera").attr("data");
	switch(idioma){
		case "uk":
			id_idioma = 1	
			break;
		case "es":
			id_idioma = 2
			break;	
	}
	//--
	if($("#form-nombre").val()==""){
		if(id_idioma == "1")
			mensaje_alerta("#campo_mensaje_clientes","Debe ingresar sus nombres", "alert-danger");
		else
			mensaje_alerta("#campo_mensaje_clientes","You must enter their names", "alert-danger");

		return false;
	}else
	if($("#form-email").val()==""){
		if(id_idioma == "1")
			mensaje_alerta("#campo_mensaje_clientes","Debe ingresar una dirección de correo v&aacute;lida", "alert-danger");
		else
			mensaje_alerta("#campo_mensaje_clientes","You must enter valid email direction", "alert-danger");
		return false;
	}
	else
	if($("#form-tlf").val()==""){
		if(id_idioma == "1")
			mensaje_alerta("#campo_mensaje_clientes","Debe ingresar un número de teléfono válido", "alert-danger");
		else
			mensaje_alerta("#campo_mensaje_clientes","You must enter valid telephone number", "alert-danger");
		return false;
	}else
	if($("#form-mensaje").val()==""){
		if(id_idioma == "1")
			mensaje_alerta("#campo_mensaje_clientes","Debe ingresar un mensaje", "alert-danger");
		else
			mensaje_alerta("#campo_mensaje_clientes","You must enter a message", "alert-danger");
		return false;
	}else
		return true;
}
//--
function insertarMensaje(){
	//--Defino el idioma
	var idioma = $("#bandera").attr("data");
	switch(idioma){
		case "uk":
			id_idioma = 2	
			break;
		case "es":
			id_idioma = 1
			break;	
	}
	//--
	validar = validar_guardar();
	if(validar==true){
		if(id_idioma == "1")
			mensaje_alerta("#campo_mensaje_clientes","Gracias por tu tiempo, tu mensaje ha sido enviado!","alert-success");
		else
			mensaje_alerta("#campo_mensaje_clientes","Thank you for your time, your message has been sent!","alert-success");
		limpiar_cajas();
	}
}
//----------


/*
*	Funcion que carga una opción de idioma u otro
*/
function cargar_web(){
	var id_idioma = $("#id_idioma").text()
	/*
	if(id_idioma==1){
		cargar_en_ingles()
	}else{
		cargar_en_espaniol()
	}
	*/
	//----------------------------------------------------------
	url = window.location.href
	var base_url = $("#base_url").val();
	$(".span-menu").removeClass("subtitulo-menu")
	switch(url){
		case base_url:
			$("#span-menu0").addClass("subtitulo-menu")
			break;
		case base_url+'inicio':
			$("#span-menu0").addClass("subtitulo-menu")
			break;
		case base_url+'home':
			$("#span-menu0").addClass("subtitulo-menu")
			break;
		case base_url+'quienes_somos':
			$("#span-menu1").addClass("subtitulo-menu2")
			break;
		case base_url+'about':
			$("#span-menu1").addClass("subtitulo-menu2")
			break;
		case base_url+'servicios':
			$("#span-menu2").addClass("subtitulo-menu2")
			break;
		case base_url+'services':
			$("#span-menu2").addClass("subtitulo-menu2")
			break;
		case base_url+'noticias':
			$("#span-menu3").addClass("subtitulo-menu2")
			break;
		case base_url+'news':
			$("#span-menu3").addClass("subtitulo-menu2")
			break;	
		case base_url+'contactanos':
			$("#span-menu4").addClass("subtitulo-menu2")
			break;
		case base_url+'contact_us':
			$("#span-menu4").addClass("subtitulo-menu2")
			break;		
		default:
			url2 = url.replace(base_url,"");
			vec_ruta = url2.split("/")
			//--
			if(vec_ruta[0]=="news"){
				$("#span-menu3").addClass("subtitulo-menu2")
			}else
			if(vec_ruta[0]=="noticias"){
				$("#span-menu3").addClass("subtitulo-menu2")
			}
			break;
			//--					
	}
	//----------------------------------------------------------
	$(".slides-pagination a").html("x");
}

//-----------------
//Cuerpo de eventos
//cargar_en_espaniol();
cargar_web();
