<?php

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class WebInicio_model extends CI_Model{
	/*
	*	consultarSlider
	*/
	public function consultarSlider($data){
		//var_dump($data);echo "<br>";
		$this->db->where('a.id_idioma', $data["id_idioma"]);
		$this->db->order_by('a.orden');
        $this->db->where('a.estatus',1);
		$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen');
		$this->db->from('slider a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
        $this->db->join('galeria c', 'c.id = a.id_imagen');
		$res = $this->db->get();
		/*var_dump($res);echo"<br>";
		var_dump($this->db->last_query());echo"<br>";*/
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
    * Para consultar publicidad
    */
    function consultarPublicidad($data){
    	$this->db->order_by('a.orden', 'ASC');
    	$this->db-> where('a.id_idioma',1);
    	$this->db-> where('a.estatus',1);
    	$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen');
    	$this->db-> from('publicidad a');
    	$this->db-> join('idioma b', 'b.id = a.id_idioma');
		$this->db-> join('galeria c', 'c.id = a.id_imagen');
		$this->db-> limit(3);

		$res = $this->db-> get();
		//print_r($this->db->last_query());die;

		if($res) {
			return $res-> result();
		} else {
			return false;
		}
    }
	/*
	*	Para consultar categorias
	*/
	function consultarCategorias($data){
    	$this->db-> where('a.estatus',1);
    	$this->db->select('a.*');
    	$this->db-> from('categorias_noticias a');
		$this->db-> limit(10);

		$res = $this->db-> get();
        
        //print_r($this->db->last_query());die;

		if($res) {
			return $res-> result();
		} else {
			return false;
		}
	}
	/*
	*	Para consultar Empresa
	*/
	function consultarEmpresa($data){
		$this->db->order_by('a.id', 'ASC');
		$this->db-> where('a.estatus',1);
    	$this->db->select('a.*, b.ruta as ruta, b.id as id_imagen');
    	$this->db-> from('empresa a');
    	$this->db-> join('galeria b', 'b.id = a.id_imagen');
		$this->db-> limit(1);

		$res = $this->db-> get();
        
        //print_r($this->db->last_query());die;

		if($res) {
			return $res-> result();
		} else {
			return false;
		}
	}
	/*
	*	Para consultar portada
	*/
	function consultarPortada($data){
		$this->db->where("a.estatus",1);
		$this->db->where("a.id_categoria",37);
		$this->db->select("a.id, a.ruta, a.titulo");
		$this->db->from("galeria a");

		$res = $this->db->get();

		if($res) {
			return $res-> result();
		} else {
			return false;
		}

	}
	/*
	*	consultarRedes
	*/
	public function consultarRedes($data){
		$this->db->order_by('a.id');
		$this->db->select('a.id, a.url_red, c.descripcion');
		$this->db->from('red_social a');
		$this->db->join('tipo_red c', 'c.id = a.id_tipo_red');

		$res = $this->db->get();
        //print_r($this->db->last_query());die;

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
	*	consultarFooter
	*/
	
	public function consultarFooter($data){
		$this->db->order_by('id','asc');
		if($data["id_footer"]!=""){
			$this->db->where('a.id', $data["id_footer"]);
		}
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma');
		$this->db->from('footer a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
		$res = $this->db->get();
        
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
	*	consultarDescripcion
	*/
	public function consultarDescripcion(){
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.descripcion as descripcion');
		$this->db->from('descripcion a');

		$res = $this->db->get();
        //print_r($this->db->last_query());die;

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
	*	consultarPalabrasClaves
	*/
	public function consultarPalabrasClaves(){
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.descripcion as palabras_claves');
		$this->db->from('palabras_claves a');

		$res = $this->db->get();
        //print_r($this->db->last_query());die;

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
	* consultarDireccion
	*/
	public function consultarDireccion($data){
		if($data["id_direccion"]!=""){
			$this->db->where('a.id', $data["id_direccion"]);
		}
		$this->db->order_by('a.orden','ASC');
        $this->db->where('a.estatus!=',2);
		//$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen');
		$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma');
		$this->db->from('direcciones a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
        //$this->db->join('galeria c', 'c.id = a.id_imagen');
		$res = $this->db->get();

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
	*	consultarTelefonos
	*/
	public function consultarTelefonos($id_direccion){
		$this->db->order_by('a.id','ASC');
        $this->db->where('a.id_direccion',$id_direccion);
		$this->db->select('a.*');
		$this->db->from('telefonos a');
		
		$res = $this->db->get();
		
		//print_r($this->db->last_query());die;
		
		if($res->num_rows()>0){
			return $res->result();
		}else{
			return false;
		}
	}

}	