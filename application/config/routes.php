<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes with
| underscores in the controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'WebInicio';
$route['404_override'] = 'WebInicio/error404';
$route['translate_uri_dashes'] = FALSE;
/*
*
*/
//---Rutas de la web
$route['productos/(:any)'] = 'WebProductos/verProducto/$1';
/*----------------------------------------------------*/
/*--------- Rutas del cms ------------------*/
$route['cms'] = 'Login';
$route['cms/dashboard'] = 'Inicio';
/*---------Nosotros------------------------*/
$route['cms/quienes_somos'] = 'QuienesSomos';
$route['cms/quienes_somos/consultar_quienes_somos']= 'QuienesSomos/consultar_nosotros';
$route['cms/quienes_somos/quienes_somos_ver']= 'QuienesSomos/nosotrosVer';

$route['cms/directiva'] = 'Directiva';
$route['cms/directiva/consultar_directiva']= 'Directiva/consultar_directiva';
$route['cms/directiva/directiva_ver']= 'Directiva/directivaVer';

$route['cms/reaseguradoras'] = 'Reaseguradoras';
$route['cms/reaseguradoras/consultar_reaseguradoras']= 'Reaseguradoras/consultar_reaseguradoras';
$route['cms/reaseguradoras/reaseguradoras_ver']= 'Reaseguradoras/reaseguradorasVer';

/*---------Slider------------------------*/
$route['cms/slider'] = 'slider';
$route['cms/slider/consultar_slider']= 'slider/consultar_slider';
$route['cms/slider/sliderVer']= 'slider/sliderVer';

/*---------Productos y servicios ------------*/
//---Productos
$route['cms/productos'] = 'productos';
$route['cms/productos/consultar_productos'] = 'productos/consultar_productos';
$route['cms/productos/productosVer'] = 'productos/productosVer';
//---Tipo de productos
$route['cms/tproductos'] = 'tproductos';
$route['cms/tproductos/consultar_tproductos'] = 'tproductos/consultar_tproductos';
$route['cms/tproductos/tproductosVer'] = 'tproductos/tproductosVer';
//---Detalles tipo de productos
$route['cms/detalle_productos'] = 'DetalleTproductos';
$route['cms/detalle_productos/consultar_productos'] = 'DetalleTproductos/consultar_detalles_productos';
$route['cms/detalle_productos/productosVer'] = 'DetalleTproductos/detalles_productosVer';
//---Servicios
$route['cms/servicios'] = 'servicios';
$route['cms/servicios/consultar_servicios'] = 'servicios/consultar_servicios';
$route['cms/servicios/serviciosVer'] = 'servicios/serviciosVer';
//---Tipo de Servicios
$route['cms/tservicios'] = 'tservicios';
$route['cms/tservicios/consultar_tservicios'] = 'tservicios/consultar_tservicios';
$route['cms/tservicios/tserviciosVer'] = 'tservicios/tserviciosVer';
//---Detalles tipo de Servicios
$route['cms/detalle_servicios'] = 'DetalleTservicios';
$route['cms/detalle_servicios/consultar_servicios'] = 'DetalleTservicios/consultar_detalles_servicios';
$route['cms/detalle_servicios/serviciosVer'] = 'DetalleTservicios/detalles_serviciosVer';
/*--------------------------------------------*/

/*---------Categorias------------------------*/
$route['cms/categorias'] = 'Categorias';
$route['cms/categorias/consultarCategorias']= 'Categorias/consultarCategorias';
$route['cms/categorias/categoriasVer']= 'categorias/categoriasVer';


/*---------Empresa------------------------*/
$route['cms/empresa'] = 'empresa';
$route['cms/empresa/consultar_empresa']= 'empresa/consultarEmpresa';
$route['cms/empresa/empresaVer']= 'empresa/empresaVer';

/*---------Noticias------------------------*/
$route['cms/noticias'] = 'noticias';
$route['cms/noticias/consultarNoticias']= 'noticias/consultarNoticias';
$route['cms/noticias/noticiasVer']= 'noticias/noticiasVer';

/*--------Publicidad-----------------------*/
$route['cms/publicidad'] = 'Publicidad';
$route['cms/publicidad/consultarPublicidad'] = 'publicidad/consultarPublicidad';
$route['cms/publicidad/publicidadVer']= 'publicidad/publicidadVer';
/*----------Categorias Noticias -----------*/
$route['cms/categorias_noticias'] = 'CategoriasNoticias';
$route['cms/categorias_noticias/consultarCategoriasNoticias']= 'CategoriasNoticias/consultarCategorias';
$route['cms/categorias_noticias/categoriasNoticiasVer']= 'CategoriasNoticias/categoriasNoticiasVer';
/*--------- -------------------------*/

/*-------- Contactos -----------------------*/
$route['cms/redes_sociales'] = 'RedesSociales';
$route['cms/contactos'] = 'Contactos';
$route['cms/footer'] = 'Footer';
$route['cms/footer/consultarFooter'] = 'footer/consultarFooter';
$route['cms/footer/footerVer']= 'footer/footerVer';

$route['cms/banco'] = 'Banco';
$route['cms/banco/consultarBanco'] = 'banco/consultarBanco';
$route['cms/banco/bancoVer'] = 'banco/bancoVer';

$route['cms/cuenta_bancaria'] = 'CuentaBancaria';
$route['cms/cuenta_bancaria/consultarCuentas'] = 'CuentaBancaria/consultarCuentas';
$route['cms/cuenta_bancaria/cuentasVer'] = 'CuentaBancaria/bancoVer';

$route['cms/direccion'] = 'Direccion';
$route['cms/direccion/consultarDireccion'] = 'direccion/consultarDireccion';
$route['cms/direccion/direccionVer'] = 'direccion/direccionVer';

/*---------Registro PDF------------------------*/
$route['cms/cargar_pdf'] = 'CargarPdf';
$route['cms/cargar_pdf/consultarPDF'] = 'CargarPdf/consultarPDF';
$route['cms/cargar_pdf/pdfVer'] = 'CargarPdf/pdfVer';
/*---------Galeria Multimedia------------------------*/
$route['cms/galeriaMultimedia'] = 'GaleriaMultimedia';
/*---------Registro Usuario CMS------------------------*/
$route['cms/registro_usuario'] = 'RegistroCMS';
$route['cms/consulta_usuariosCMS'] = 'RegistroCMS/consultar_usuarios';
$route['cms/consulta_usuariosCMS/modificar'] = 'RegistroCMS/usuariosVer';
/*---------Descripcion------------------------*/
$route['cms/descripcion'] = 'Descripcion';
/*---------Palabras claves------------------------*/
$route['cms/palabras_claves'] = 'Palabras_claves';
/*---------Auditoria------------------------*/
$route['cms/auditoria'] = 'RegistroCMS/auditoria';
/*---------Bancis------------------------*/
$route['cms/categorias/consultarCategorias']= 'Categorias/consultarCategorias';
$route['cms/categorias/categoriasVer']= 'categorias/categoriasVer';
/*----------------------------------------------------*/
//--Página de errores CMS:
$route['cms/no_tiene_permisos'] = 'error';
/***/