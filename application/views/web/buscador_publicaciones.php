<div id="divDetalle" class="container mt-5" ng-controller="buscadorController">
  <input type="hidden" name="tipo_filtro" id="tipo_filtro" value="<?=$tipo?>">
  <input type="hidden" name="id_filtro" id="id_filtro" value="<?=$id?>">
  <section>
    <div class="row mb-5">
      <div class="col-lg-12">
        <h2>
          <p ng-if="tipo=='categorias'">
            Post por categoría:
          </p>
          <p ng-if="tipo=='titulos'">
            Búsqueda por títulos:
          </p>
          <p ng-if="mensaje!=''">
            {{mensaje}}
          </p>
        </h2>
      </div>
      <div class="col-lg-9 col-md-12 col-xs-12 col-sm-12 mt-5 mb-5">
          <div class="row">
              <!-- -->
              <div class="pt-20 col-lg-6 col-md-12 col-xs-12 col-sm-12 mb-5"
              ng-repeat="post in posts_buscador track by $index" ng-if="mensaje==''">
                <div class="card card-body text-center wow fadeInUp">
                  <div class="card-ribbon card-ribbon-top card-ribbon-right bg-faded ">
                    <div class="icon-box-icon">
                      <img class="img-responsive img-buscar"
                      ng-src="{{base_url}}{{post.ruta}}">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <h4 class="card-title">
                      {{post.titulo}}
                    </h4>
                  </div>
                  <div class="col-lg-12">   
                    <p id="autor" name="autor" class="autor">
                      <i class="fa fa-user-o" aria-hidden="true"></i>
                      {{post.autor}}
                    </p>
                  </div>
                  <div class="col-lg-12"> 
                    <p id="calendario_post" name="calendario_post" class="autor"> 
                      <i class="fa fa-calendar"></i>
                      {{post.fecha}}
                    </p>
                    <p id="categoria_post" name="categoria_post" class="autor"> 
                      <i class="fa fa-tasks"></i>
                      {{post.categoria_noticia}}
                    </p>
                  </div>  
                  <p ng-bind-html="post.descripcion_sin_html">
                  </p>
                  <a ng-if="post.editorial!='1'" href="{{base_url}}publicaciones/{{post.slug}}"
                  class="btn btn-brand btn-sm mt-3">Leer mas</a>
                  <a ng-if="post.editorial=='1'" href="{{base_url}}editorial/{{post.slug}}"
                  class="btn btn-brand btn-sm mt-3">Leer mas</a>
                </div>
              </div>
              <!-- -->
              <div style="clear:both"></div>
          </div>
        <!-- Mas posts -->
        <div class="media flex-wrap productBox mt-5 mb-5">
          <!-- -->
          <div class="pt-20 col-lg-6 col-md-12 col-xs-12 col-sm-12 mb-5"
          ng-repeat="post2 in posts_buscador_mas track by $index" ng-if="mensaje==''">
            <div class="card card-body text-center wow fadeInUp">
              <div class="card-ribbon card-ribbon-top card-ribbon-right bg-faded ">
                <div class="icon-box-icon">
                  <img class="img-responsive img-buscar"
                  ng-src="{{base_url}}{{post2.ruta}}">
                </div>
              </div>
              <div class="col-md-12">
                <h4 class="card-title">
                  {{post2.titulo}}
                </h4>
              </div>
              <div class="col-lg-12">   
                <p id="autor" name="autor" class="autor">
                  <i class="fa fa-user-o" aria-hidden="true"></i>
                  {{post2.autor}}
                </p>
              </div>
              <div class="col-lg-12"> 
                <p id="calendario_post" name="calendario_post" class="autor"> 
                  <i class="fa fa-calendar"></i>
                  {{post2.fecha}}
                </p>
                <p id="categoria_post" name="categoria_post" class="autor"> 
                  <i class="fa fa-tasks"></i>
                  {{post2.categoria_noticia}}
                </p>
              </div>  
              <p ng-bind-html="post2.descripcion_sin_html">
              </p>
              <a href="{{base_url}}publicaciones/{{post2.slug}}"
              class="btn btn-brand btn-sm mt-3">Leer mas</a>
            </div>
          </div>
          <!-- -->
          <div style="clear:both"></div>
        </div>
        <!-- -->
        <div class="col-lg-12">
          <div  id="campo_mensaje_noticias" name="campo_mensaje_noticias" ></div>
        </div>
        <!-- -->
        <div class="row centrado boton-nosotros" ng-if="mensaje==''">
          <div class="col-md-12">
            <div class="text-center">
              <div class="btn btn-round btn-lg btn-brand" ng-click="cargar_mas_subnoticias()">Ver mas Post
              </div>
            </div>
          </div>
          <div style="clear: both;"></div>
        </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 cuerpo-detalle-publi">
        <div class="container">
            <busca-dor></busca-dor>
            
            <ca-tegorias></ca-tegorias>
            
            <div class="col-lg-12">
              <h3 class="h3-title">Publicidad</h3>
            </div>

            <publi-cidad></publi-cidad>
        </div>
      </div>
    </div>
  </section>
  <section class="cuerpo-detalle-publi-movil">
      <!-- -->
      <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 ">
      <div class="container">
        
        <busca-dor></busca-dor>

        <ca-tegorias></ca-tegorias>

        <div class="col-lg-12">
          <h3 class="h3-title">Publicidadx</h3>
        </div>
        
        <publi-cidad ng-repeat="publicidad in publicidades track by $index"></publi-cidad>

      </div>
    </div>
      <!-- -->
    </section>
</div>      