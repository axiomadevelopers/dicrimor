<div id="divInicio" class="container" ng-controller="mainController">
	<section>
		<div class="row">
			<!-- Productos -->
  			<div class="col-lg-6 col-md-6 col-sm-6 col-6 wow fadeInUp mb-5" ng-repeat="producto in productos track by $index">
				<div class="col-12">
					<div class="row">
						<div class="thumbnail centrado-base">
							<div class="order-1">
								<img ng-src="{{producto.imagen}}" class="img-responsive img-productos img-productos-h" alt="{{producto.titulo}}">
								<a href="{{base_url}}{{producto.slug}}"
											class="btn btn-brand btn-block btn-producto btn-lg">
											{{producto.titulo}}
								</a>
								<div style="clear:both"></div>
							</div>
						</div>
					</div>	
				</div>
  			</div>
			  <!-- Productos -->
		</div>	  
	</section>					
</div>
