<div ng-controller="productosController">
    <section class="mainContent clearfix">
        <div class="container">
            <div class="row singleProduct">
                <div class="col-md-12 mb-5">
                    
                    <div class="media flex-wrap media-productos">
                    
                        <div class="media-left productSlider">
                            <!-- Imagenes principal -->
                            <div id="slider-ppal">
                            <!-- -->
                                <div class="row">
                                    <div class="large-12 columns columna-img">
                                        <div class="col-lg-12">
                                        <div id="slider" class="">
                                            <div class="item img-slider-ppal" id="div_imagen1" ><!-- onmouseover="super_hover()"-->
                                                <img id="imagen1" ng-src="{{producto_seleccionado.imagen}}" class="img-ppal-producto">
                                                <!-- height:501px;margin-right:2px onmouseover="super_hover()"-->
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            <!-- -->  
                            </div>   
                        </div>
                        <br>
                        <div class="media-body cuerpo-detalle-producto">
                            <form id="formProducto" name="formProducto" target="_self">
                                <h2 class="titulo_producto">{{producto_seleccionado.titulo}}</h2>
                                <h3 class="titulo-precio">{{producto_seleccionado.precio}}</h3>
                                <div class="">
                                </div>
                                <div class="tabArea mb-5">
                                    <ul class="lista-productos">
                                        <li class="ul_productos" ng-repeat="prod in producto_seleccionado.descripcion track by $index">
                                        {{prod}}
                                        </li>
                                    </ul>
                                    <div class="col-lg-12">
                                        <div id="campo_mensaje_productos" name="campo_mensaje_productos" ></div>
                                    </div>
                                    
                                </div>
                            </form>  
                        </div>
                    </div>
                    <a href="{{base_url}}"class="btn btn-producto2 btn-rounded btn-brand">
                    Volver
                    </a>
                </div>
            </div>
            <!-- -->
            <div id="slug_producto" name="slug_producto" style="display: none;" value="<?php echo $slug; ?>"></div>
            <div id="id_producto" name="id_producto" style="display: none;"></div>
        </div>
    </section>
</div>
