<!-- Fin de wrapper -->
</div>
<!-- FIn de box -->
</div>
       
<!-- Parallax de servicios-->
    <div name="id_idioma" id="id_idioma" style="display: none"><?php echo $id_idioma;?></div>
    <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>"> 		
		<!-- Footer-->
	<footer class="dark-bg fadeInUp wow" id="footer" ng-controller="footerController">
        <div class="container inner">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-6 inner parrafos parrafos-footer">
                    <div >
                        <!--<img src="<?=base_url();?>assets/images/logo_peque.png" class="logo img-intext" alt="">-->
                        <h2 class="title-webzine">{{footer.titulo}}</h2>
                    </div>
                   <div ng-bind-html="footer.direccion"></div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-6  inner texto-footer">
                    <h4 class="subtitulos">PRODUCTOS POPULARES</h4>
                    <div>
                        <ul class="padding0 parrafos parrafos-footer" ng-repeat="producto in productos track by $index">
                            <li><span class="icono_serv"></span>{{ producto.titulo }}</li>                      
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-6  inner texto-footer">
                    <h4 class="subtitulos">CONTÁCTANOS</h4>
                    <ul class="contacts parrafos parrafos-footer">
                       
                        <li><i class="icon-mobile contact"></i>{{footer.telefono}}</li>
                        <li><a href="mailto:{{footer.correo}}" target="_blank"><i class="icon-mail-1 contact correo-mail"></i>{{footer.email}}</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-6  inner texto-footer">
                    <h4 class="subtitulos">REDES SOCIALES</h4>
                        <ul class="social parrafos parrafos-footer redes-f" style="text-align: center">
                            <li class="left social_li2"><a href="{{facebook}}" target="_blank"><i class="icon-facebook-circled"></i></a></li>
                            <li class="left social_li2"><a href="{{twitter}}" target="_blank"><i class="icon-twitter-circled"></i></a></li>
                            <li class="left social_li2"><a href="{{linkedin}}" target="_blank"><i class="icon-linkedin-circled"></i></a></li>
                            <li class="left social_li2"><a href="{{instagram}}" target="_blank"><i class="icon-s-instagram"></i></a></li>
                        </ul>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container inner">
                <p class="pull-center footer">© 2021 Dicrimor, todos los derechos Reservados, desarrollado por <a href="http://www.axiomadevelopers.com.ve" target="_blank">Axioma Developers.</a></p>
            </div>
        </div>
    </footer>
		<!-- Footer end-->

		<div class="scroll-top" onclick="subir_top()"><i class="fa fa-angle-up"></i></div>
	</div>
	<!-- Wrapper end-->
</div>
		<!-- Layout end-->

<!-- Off canvas-->
<div class="off-canvas-sidebar">
	<div class="off-canvas-sidebar-wrapper">
		<div class="off-canvas-header"><a class="close-offcanvas" href="index-21.html#"><span class="arrows arrows-arrows-remove"></span></a></div>
		
	</div>
</div>
<!-- Off canvas end-->
</body>
<!-- Scripts-->
<script src="<?=base_url();?>assets/web/jquery-2.2.4.min.js"></script>
<script src="<?=base_url();?>assets/web/popper.min.js"></script>
<script src="<?=base_url();?>assets/web/bootstrap/js/bootstrap.min.js"></script>
<!--
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
-->

<!--
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDlYNNs7VBO71qiKFMNiD0R9sd8hOt0wD4"></script>
-->
<script src="<?=base_url();?>assets/web/js/plugins.min.js"></script>
<script src="<?=base_url();?>assets/web/js/custom.js"></script>

<!--Plugins -->
<script src="<?=base_url();?>assets/web/plugins/dzsparallaxer/dzsparallaxer.js"></script>
<script src="<?=base_url();?>assets/web/plugins/dzsparallaxer/dzsscroller/scroller.js"></script>
<script src="<?=base_url();?>assets/web/plugins/dzsparallaxer/advancedscroller/plugin.js"></script>
<script src="<?=base_url();?>assets/web/plugins/wow/wow.min.js"></script>
<script src="<?=base_url();?>assets/web/plugins/fancybox/jquery.fancybox.min.js"></script>
<script src="<?=base_url();?>assets/web/plugins/slick/js/slick.js"></script>
<script src="<?=base_url();?>assets/web/js/fbasic.js"></script>

<!--Angular Core -->
<!--
<!--Core Angular JS -->
<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/angular.js"></script>
<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/angular-route.js"></script>
<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/angular-sanitize.min.js"></script>
<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/app.js"></script>
<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/directivas/directives.js"></script>
<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/servicios/services.js"></script>
<!--Controladores Ng -->
<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/mainController.js"></script>
<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/inicioController.js"></script>
<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/productosController.js"></script>
<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/buscadorController.js"></script>
<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/footerController.js"></script>

</html>
