<div id="preloader_zougzoug" class="" >
  <div class="loading-spiner">
    <div class="container">
        <div class="row">
          <div id="loader-container"
            style="width:100%;height:auto;background-image:url({{base_url}}{{empresa.ruta}});background-position:center;background-repeat:no-repeat;" class="col-12">
            <div class="loader"></div>
            <div class="col-12"> 
              <img ng-src="<?=base_url();?>/assets/web/images/preloader.gif" class="img-responsive"
                style="margin: 0 auto;display: flex;">
            </div>
            <div style="clear:both"></div>
          </div>
          
        </div>  
    </div>
  </div>
</div>
<!-- Layout-->
<div class="layout">
<header class="header header-right undefined invisible">
	<div class="container-fluid">
		<div class="inner-header">
			<a class="inner-brand a-menu0"> 
				<img class="brand-dark img-header" ng-src="{{base_url}}{{empresa.ruta}}" alt="">
				<img class="brand-light img-header" ng-src="{{base_url}}{{empresa.ruta}}" alt="">
			</a>
		</div>
	</div>
</header>
<!-- -->
<!-- Wrapper-->
<div class="wrapper" ng-controller="inicioController">
    <!-- Widget: user widget style 1 -->
    <div class="box box-widget widget-user">
        <div class="img-code widget-user-header bg-black" style="background: url({{empresa.portada}}) center center;">
            <h5 class="widget-user-desc"></h5>
        </div>
        <div class="widget-user-image">
          <p class="img-center-zine">
            <a href="{{base_url}}">
              <img class="img-circle2" ng-src="{{empresa.logo}}" alt="User Avatar">
            </a>
          </p> 
        </div>

        <div class="box-footer mt-5">
          <div class="row row-cv">
            <div class="col-sm-12 border-right centrado">
              <div class="description-block centrado">
                <div class="texto-centrado">
                    <h2 class="widget-user-username title-webzine">
                      {{empresa.titulo}}
                    </h2>
                    <h4>
                      <span id="subtitulo-zine" class="description-text font-titulos">
                      {{empresa.sub_titulo}}
                      </span>
                    </h4>
                    <h4>
                      <span id="subtitulo-zine" class="description-text font-titulos">
                      {{empresa.sub_titulo2}}
                      </span>
                    </h4>
                </div>
                <div class="" style="clear:both"></div>   
              </div>
                <!-- /.description-block -->
            </div>    
                <!--Fin descripción -->
                <div style="clear: both"></div>
          </div>
        </div>