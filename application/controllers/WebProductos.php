<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class WebProductos extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
    }
    /*
    * Index
    */
    public function index(){}
    /*
    * verProductos
    */
    public function verProducto($slug){
      $idioma = 1;
      $datos["slug"] = $slug;
      #Verifico si existe el slug
      if($datos["slug"]){
        $this->load->view('/web/header');
        $this->load->view('/web/menu');
        $this->load->view('/web/detalle_producto',$datos);
        $this->load->view('/web/footer');
      }else{
        $this->error404();
      }
      //---
     
    }
    /*
    *
    */
    public function error404(){
        $idioma = 1;
        //---
        $datos="";
        $this->load->view('/web/header');
        $this->load->view('/web/error404',$datos);
        $this->load->view('/web/footer');
    }
}    