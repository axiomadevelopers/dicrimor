<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class WebPublicacion extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->model('WebPublicacion_model');
      $this->load->helper('meses');
    }
    /*
    * Index
    */
    public function index(){}
    /*
    * verPublicacion
    */
    public function verPublicacion($slug){
      $idioma = 1;
      $datos["slug"] = $slug;
      $datos["editorial"] = "";
      #Verifico si existe el slug
      $existe_slug = $this->WebPublicacion_model->consultarPublicacionSlugExiste($datos["slug"]);
      if($existe_slug>0){
        $this->load->view('/web/header');
        $this->load->view('/web/menu');
        $this->load->view('/web/detalle_publicacion',$datos);
        $this->load->view('/web/footer');
      }else{
        $this->error404();
      }
      //---
     
    }
    /*
    *
    */
    public function verEditorial($slug){
        $idioma = 1;
        $datos["slug"] = $slug;
        $datos["editorial"] = "1";
        #Verifico si existe el slug
        $existe_slug = $this->WebPublicacion_model->consultarPublicacionSlugExiste($datos["slug"]);
        if($existe_slug>0){
            $this->load->view('/web/header');
            $this->load->view('/web/menu');
            $this->load->view('/web/detalle_publicacion',$datos);
            $this->load->view('/web/footer');
        }else{
            $this->error404();
        }
    }
    /*
    * BUscador
    */
    public function buscador(){
      $idioma = 1;
      $datos = "";
      //---
      $this->load->view('/web/header');
      $this->load->view('/web/menu');
      $this->load->view('/web/buscador_publicaciones',$datos);
      $this->load->view('/web/footer');
    }

    /*
    *   buscadorTipo
    */
    public function buscadorTipo($tipo,$id){
      $datos["tipo"] = $tipo;
      if($tipo=="titulos"){
        $datos["id"]= urldecode($id);
      }else{
        $datos["id"] = $id;
      }
      //var_dump($datos["id"]);die;
      //---
       $this->load->view('/web/header');
       $this->load->view('/web/menu');
       $this->load->view('/web/buscador_publicaciones',$datos);
       $this->load->view('/web/footer');
       //--- 
    }
    /*
    * Función para consultarNoticias
    */
    public function consultarNoticias(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebPublicacion_model->consultarNoticias($datos);
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            $fecha = $value->fecha;
            $vector_fecha = explode("-",$fecha);
            $valor->dias = $vector_fecha[2];
            $valor->mes = strtoupper(meses($vector_fecha[1]));
            $valor->fecha = $vector_fecha[2]."/".$vector_fecha[1]."/".$vector_fecha[0];
            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
            /***/
            if(($value->autor=="administrador")&&($datos["id_idioma"]=="2")){
                $value->autor = "administrator";
            }
            /***/
            $res[] = $valor;
        }
        $listado = (object)$res;
        //var_dump($listado);die();
        die(json_encode($listado)); 
    }
    /*
    * Función consultarNoticiasFiltros
    */
    public function consultarNoticiasFiltros(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebPublicacion_model->consultarNoticiasFiltros($datos);
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            $fecha = $value->fecha;
            $vector_fecha = explode("-",$fecha);
            $valor->dias = $vector_fecha[2];
            $valor->mes = strtoupper(meses($vector_fecha[1]));
            $valor->fecha = $vector_fecha[2]."/".$vector_fecha[1]."/".$vector_fecha[0];
            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
            /***/
            if(($value->autor=="administrador")&&($datos["id_idioma"]=="2")){
                $value->autor = "administrator";
            }
            /***/
            $res[] = $valor;
        }
        $listado = (object)$res;
        die(json_encode($listado)); 
    }
    /*
    *
    */
    public function consultarNoticiasBuscador(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebPublicacion_model->consultarNoticiasBuscador($datos);
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            $fecha = $value->fecha;
            $vector_fecha = explode("-",$fecha);
            $valor->dias = $vector_fecha[2];
            $valor->mes = strtoupper(meses($vector_fecha[1]));
            $valor->fecha = $vector_fecha[2]."/".$vector_fecha[1]."/".$vector_fecha[0];
            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
            /***/
            if(($value->autor=="administrador")&&($datos["id_idioma"]=="2")){
                $value->autor = "administrator";
            }
            /***/
            $res[] = $valor;
        }
        $listado = (object)$res;
        die(json_encode($listado));
    }
    /*
    *
    */
    public function error404(){
        $idioma = 1;
        //---
        $datos="";
        $this->load->view('/web/header');
        $this->load->view('/web/error404',$datos);
        $this->load->view('/web/footer');
    }
}    